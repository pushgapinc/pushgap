var cluster = require('cluster');
var path = require('path');
var winston = require(path.join(__dirname, "application", "utils", "winston"));

if(cluster.isMaster) {
    var numWorkers = require('os').cpus().length;

    winston.info('Master cluster setting up ' + numWorkers + ' workers...');

    for(var i = 0; i < numWorkers; i++) {
        cluster.fork();
    }

    cluster.on('online', function(worker) {
        winston.info('Worker ' + worker.process.pid + ' is online');
    });

    cluster.on('exit', function(worker, code, signal) {
        winston.info('Worker ' + worker.process.pid + ' died with code: ' + code + ', and signal: ' + signal);
        winston.info('Starting a new worker');
        cluster.fork();
    });
} else {


    /*START: module includes*/
    var express = require('express');
    var cors = require('cors');
    var morgan = require('morgan');
    var bodyParser = require('body-parser');
    var mongoose = require('mongoose');
    var compression = require('compression');
    var ejs = require('ejs');
    var subdomain = require('express-subdomain');
    var methodOverride = require('method-override');
    var config = require(path.join(__dirname, "application", "config", "config"));
    var apiRouter = require(path.join(__dirname, "application", "routes", "apiRouter"));
    var sdkRouter = require(path.join(__dirname, "application", "routes", "sdkRouter"));
    var portalRouter = require(path.join(__dirname, "application", "routes", "portalRouter"));
    var defaultRouter = require(path.join(__dirname, "application", "routes", "defaultRouter"));
    var apiResponseHelper = require(path.join(__dirname, 'application', 'helpers', 'apiResponseHelper'));
    var utilsHelper = require(path.join(__dirname, 'application', 'helpers', 'utilsHelper'));
    /*END: module includes*/

    var app = express();

    /*START: middleware*/
    app.use(cors());
    app.use(compression());
    app.use(morgan("dev", { "stream": winston.stream }));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(methodOverride('X-HTTP-Method-Override'));
    app.enable('trust proxy');
    /*END: middleware*/

    /*START: template engine*/
    app.set('views', path.join(__dirname, 'application', 'templates'));
    app.engine('html', ejs.renderFile);
    app.set('view engine', 'html');
    /*END: template engine*/

    /*START: mongoose connection*/
    mongoose.Promise = global.Promise;
    mongoose.connect(config.database.url, config.database.options);
    mongoose.connection.on('connected', function () {
        winston.info('Mongoose default connection open to ' + config.database.url);
    });
    mongoose.connection.on('error', function (err) {
        winston.info('Mongoose default connection error: ' + err);
        mongoose.disconnect();
    });
    mongoose.connection.on('disconnected', function () {
        winston.info('Mongoose default connection disconnected');
        process.exit(0);
    });
    process.on('SIGINT', function () {
        mongoose.connection.close(function ()
        {
            winston.info('Mongoose default connection disconnected through app termination');
            process.exit(0);
        });
    });
    /*END: mongoose connection*/

    /*START: static routes served by nginx*/
    //app.use('/resources', express.static(path.join(__dirname, 'application', 'static', 'public')));
    /*END: static routes served by nginx*/

    /*START: routes*/
    app.use(subdomain('api', apiRouter));
    app.use(subdomain('sdk', sdkRouter));
    app.use(subdomain('*', portalRouter));
    app.use('/', defaultRouter);
    /*END: routes*/

    /*START: handle 404 not found*/
    app.use(function (req, res, next)
    {
        apiResponseHelper.sendResponse(res, 404, false, "404 not found.");
    });
    /*END: handle 404 not found*/

    /*START: handle exceptions*/
    app.use(function (err, req, res, next)
    {
        apiResponseHelper.sendResponse(res, err.status | 500, false, err.message, null, err.stack);
    });
    /*END: handle exceptions*/

    /*START: listen to port*/
    app.listen(config.server.port, function ()
    {
        winston.info("server is up and running on port " + config.server.port + " in " + config.server.env + " mode");
    });
    /*END: listen to port*/


}
