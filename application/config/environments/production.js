var config = {};

/*START: database configuration*/
config.database = {};
config.database.type = 'mongodb';
config.database.dbname = 'pushgap';
config.database.username = 'admin';
config.database.password = 'admin';
config.database.host = "localhost";
config.database.port = "27017";
config.database.options =
{
	server: {
		socketOptions: {
			keepAlive: 1
		}
	}
};
config.database.url
	= config.database.type + "://" + config.database.username + ":" + config.database.password + "@" + config.database.host + ":" + config.database.port + "/" + config.database.dbname;
/*END: database configuration*/

/*START: email server configuration*/
config.email = {};
config.email.smtp = {};
config.email.settings = {};
config.email.smtp.host = 'smtp.mailgun.org';
config.email.smtp.port = 465;
config.email.smtp.username = 'admin@pushgap.com';
config.email.smtp.password = 'Ironman@h4ck';
config.email.settings.defaultEmail = 'admin@pushgap.com';
/*END: email server configuration*/

/*START: json web token configuration*/
config.jwt = {};
config.jwt.secret = "7854jgkfdg854jgkfdjg895483jgkfdjgwqaseaads";
config.jwt.options = {};
config.jwt.options.expiresIn = 60 * 60; //in seconds
/*END: json web token configuration*/

/*START: push configuration*/
config.push = {};
config.push.gcm_api_key = 'AIzaSyBJoLxNzvQFTfLo3vP3ZgnKS4fTnugkIF0';
/*END: push configuration*/

module.exports = config;