/*START: module includes*/
var path = require('path');
/*END: module includes*/

var config = {};

//load environment specific configuration
config = require(path.join(__dirname, "environments", process.env.NODE_ENV));

/*START: set server configuration based on environment variables*/
config.server = {};
config.server.env = process.env.NODE_ENV;
config.server.port = process.env.NODE_PORT;
/*END: set server configuration based on environment variables*/

/*START: app settings*/
config.app = {};
config.app.name = 'PushGap';
/*END: app settings*/

/*START: disallowed subdomains*/
config.disallowedSubdomains = ['cdn', 'sdk', 'demo', 'docs', 'documentation', 'images', 'pushgap', 'api'];
/*END: disallowed subdomains*/

/*START: plans*/
config.pricingModel = {};
config.pricingModel.plans = [];
var freeTrial = {};
freeTrial.id = 'trial';
freeTrial.name = 'Free Trial';
freeTrial.amount = 0;
freeTrial.configuration = {};
freeTrial.configuration.maxApplications = 1;
freeTrial.configuration.maxSubscriptions = 500;

var basicPlan = {};
basicPlan.id = 'basic';
basicPlan.name = 'Basic';
basicPlan.amount = 24.99;
basicPlan.configuration = {};
basicPlan.configuration.maxApplications = 3;
basicPlan.configuration.maxSubscriptions = 5000;

var premiumPlan = {};
premiumPlan.id = 'premium';
premiumPlan.name = 'Premium';
premiumPlan.amount = 49.99;
premiumPlan.configuration = {};
premiumPlan.configuration.maxApplications = 10;
premiumPlan.configuration.maxSubscriptions = 20000;

config.pricingModel.plans.push(freeTrial);
config.pricingModel.plans.push(basicPlan);
config.pricingModel.plans.push(premiumPlan);
/*END: plans*/
module.exports = config;
