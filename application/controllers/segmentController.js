/*START: module includes*/
var segmentController = {};
var path = require('path');
var emailHelper = require(path.join(__dirname, '..', 'helpers', 'emailHelper'));
var apiResponseHelper = require(path.join(__dirname, '..', 'helpers', 'apiResponseHelper'));
var utilsHelper = require(path.join(__dirname, '..', 'helpers', 'utilsHelper'));
var Segment = require(path.join(__dirname, '..', 'models', 'segment'));
var Subscription = require(path.join(__dirname, '..', 'models', 'subscription'));
var $q = require('q');
/*END: module includes*/

/*START: create */
segmentController.create = function(req, res, next) {
    if (!utilsHelper.isValidObjectId(req.params.workspace_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid workspace id.');
        return;
    }
    if (!utilsHelper.isValidObjectId(req.params.application_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid application id.');
        return;
    }
    if (!req.body.name) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid segment name.');
        return;
    }
    if (!req.body.definition) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid segment definition.');
        return;
    }
    if (!req.body.definition.conditions) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid segment conditions.');
        return;
    }
    if (req.body.definition.conditions.length == 0) {
        apiResponseHelper.sendResponse(res, 400, false, 'segment should have at least one condition.');
        return;
    }
    req.body.workspace_id = req.params.workspace_id;
    req.body.application_id = req.params.application_id;

    new Segment(req.body).save(function(err, segment) {
        if (err) {
            var mongooseError = utilsHelper.getMongooseError(err);
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
        }
        else {
            apiResponseHelper.sendResponse(res, 200, true, 'segment created successfully.', segment);
        }
    });
};
/*END: create */

/*START: edit */
segmentController.edit = function(req, res, next) {
    if (!utilsHelper.isValidObjectId(req.params.workspace_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid workspace id.');
        return;
    }
    if (!utilsHelper.isValidObjectId(req.params.application_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid application id.');
        return;
    }
    if (!utilsHelper.isValidObjectId(req.params.segment_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid segment id.');
        return;
    }
    if (!req.body.name) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid segment name.');
        return;
    }
    if (!req.body.definition) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid segment definition.');
        return;
    }
    if (!req.body.definition.conditions) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid segment conditions.');
        return;
    }
    if (req.body.definition.conditions.length == 0) {
        apiResponseHelper.sendResponse(res, 400, false, 'segment should have at least one condition.');
        return;
    }

    Segment.findOne({workspace_id: req.params.workspace_id, application_id: req.params.application_id, _id: req.params.segment_id},function(err, segment) {
        if (err) {
            var mongooseError = utilsHelper.getMongooseError(err);
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
        }
        else {
           if (segment) {
                segment.name = req.body.name;
                segment.definition.conditions = req.body.definition.conditions;
               segment.save(function(err, segment) {
                   if (err) {
                       var mongooseError = utilsHelper.getMongooseError(err);
                       apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
                   }
                   else {
                       apiResponseHelper.sendResponse(res, 200, true, 'segment updated successfully.', segment);
                   }
               });
           }
            else {
               apiResponseHelper.sendResponse(res, 400, false, 'segment not found.');
           }
        }
    });


};
/*END: edit */

/*START: get */
segmentController.get = function(req, res, next) {
    if (!utilsHelper.isValidObjectId(req.params.workspace_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid workspace id.');
        return;
    }
    if (!utilsHelper.isValidObjectId(req.params.application_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid application id.');
        return;
    }

    Segment.find({workspace_id: req.params.workspace_id, application_id: req.params.application_id}, function(err, segments) {
        if (err) {
            var mongooseError = utilsHelper.getMongooseError(err);
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
        }
        else {
            apiResponseHelper.sendResponse(res, 200, true, 'application segments.', segments);
        }
    });
};
/*END: get */

/*START: delete */
segmentController.delete = function(req, res, next) {
    if (!utilsHelper.isValidObjectId(req.params.workspace_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid workspace id.');
        return;
    }
    if (!utilsHelper.isValidObjectId(req.params.application_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid application id.');
        return;
    }
    if (!utilsHelper.isValidObjectId(req.params.segment_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid segment id.');
        return;
    }
    Segment.remove({workspace_id: req.params.workspace_id,application_id: req.params.application_id, _id: req.params.segment_id}, function(err, deleteCount) {
        if (err) {
            var mongooseError = utilsHelper.getMongooseError(err);
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
        }
        else {
            apiResponseHelper.sendResponse(res, 200, true, 'segment deleted successfully.');
        }
    });
};
/*END: delete */

/*START: getSegmentSubscriptionCount */
segmentController.getSegmentSubscriptionCount = function(req, res, next) {
    if (!utilsHelper.isValidObjectId(req.params.workspace_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid workspace id.');
        return;
    }
    if (!utilsHelper.isValidObjectId(req.params.application_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid application id.');
        return;
    }
    var query = {};
    query.workspace_id = req.params.workspace_id;
    query.application_id = req.params.application_id;




    if (req.body.definition && req.body.definition.conditions) {
        req.body.definition.conditions.forEach(function(condition) {
            if (condition.filter == 'browser') {
                query.browser = condition.value;
            }
            else if (condition.filter == 'is_mobile') {
                query.is_mobile = condition.value;
            }
            else if (condition.filter == 'tags') {
                if (condition.operator == 'containing_one_of') {
                    query.$or = [];
                    var tags = condition.value.split(";");
                    tags.forEach(function(tag) {
                        query.$or.push({
                            'tags': tag
                        });
                    });
                }
                if (condition.operator == 'containing_all') {
                    query.$and = [];
                    var tags = condition.value.split(";");
                    tags.forEach(function(tag) {
                        query.$and.push({
                            'tags': tag
                        });
                    });
                }
            }
        });
    }




    Subscription.count(query, function(err, count) {
        if (err) {
            var mongooseError = utilsHelper.getMongooseError(err);
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
        }
        else {
            apiResponseHelper.sendResponse(res, 200, true, 'subscription count.', count);
        }
    });
};
/*END: getSegmentSubscriptionCount */

module.exports = segmentController;