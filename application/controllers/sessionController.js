/*START: module includes*/
var sessionController = {};
var path = require('path');
var config = require(path.join(__dirname, '..', 'config', 'config'));
var apiResponseHelper = require(path.join(__dirname, '..', 'helpers', 'apiResponseHelper'));
var utilsHelper = require(path.join(__dirname, '..', 'helpers', 'utilsHelper'));
/*END: module includes*/

/*START: getCurrentUser */
sessionController.getCurrentUser = function(req, res, next) {

    apiResponseHelper.sendResponse(res, 200, true, 'current user details.', req.key);
};
/*END: getCurrentUser */

module.exports = sessionController;