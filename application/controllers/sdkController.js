/*START: module includes*/
var sdkController = {};
var path = require('path');
var config = require(path.join(__dirname, '..', 'config', 'config'));
var utilsHelper = require(path.join(__dirname, '..', 'helpers', 'utilsHelper'));
/*END: module includes*/

/*START: pushgap_sdk*/
sdkController.pushgap_sdk = function(req, res, next) {
    res.render('sdk/pushgap_sdk', {workspaceId: req.query.workspace_id, applicationId: req.query.application_id, api: req.protocol + '://' + req.headers.host.replace('sdk', 'api'), host: req.query.host});
};
/*END: pushgap_sdk*/

/*START: pushgap_client_sdk*/
sdkController.pushgap_client_sdk = function(req, res, next) {
    res.type('text/javascript');
    res.render('sdk/pushgap_client_sdk', {applicationId: req.query.application_id, host: req.protocol + '://' + req.headers.host.replace('sdk', req.query.domain), domain: req.query.domain});
};
/*END: pushgap_client_sdk*/

module.exports = sdkController;