/*START: module includes*/
var applicationController = {};
var path = require('path');
var config = require(path.join(__dirname, '..', 'config', 'config'));
var apiResponseHelper = require(path.join(__dirname, '..', 'helpers', 'apiResponseHelper'));
var utilsHelper = require(path.join(__dirname, '..', 'helpers', 'utilsHelper'));
var Application = require(path.join(__dirname, '..', 'models', 'application'));
var Subscription = require(path.join(__dirname, '..', 'models', 'subscription'));
var Notification = require(path.join(__dirname, '..', 'models', 'notification'));
var Segment = require(path.join(__dirname, '..', 'models', 'segment'));
/*END: module includes*/

/*START: create */
applicationController.create = function(req, res, next) {

    if (!utilsHelper.isValidObjectId(req.params.workspace_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid workspace id.');
        return;
    }
    if (!req.body.name) {
        apiResponseHelper.sendResponse(res, 400, false, 'application name is required.');
        return;
    }
    if (!req.body.domain) {
        apiResponseHelper.sendResponse(res, 400, false, 'application domain is required.');
        return;
    }
    if (config.disallowedSubdomains.indexOf(req.body.domain) != -1) {
        apiResponseHelper.sendResponse(res, 400, false, req.body.domain + ' - domain is not allowed.');
        return;
    }
    if (!req.body.website) {
        apiResponseHelper.sendResponse(res, 400, false, 'website information is required.');
        return;
    }
    if (!req.body.website.protocol) {
        apiResponseHelper.sendResponse(res, 400, false, 'website protocol is required.');
        return;
    }
    if (!req.body.website.url) {
        apiResponseHelper.sendResponse(res, 400, false, 'website url is required.');
        return;
    }
    if (!req.body.gcm_sender_id) {
        apiResponseHelper.sendResponse(res, 400, false, 'gcm_sender_id is required.');
        return;
    }
    if (!req.body.gcm_api_key) {
        apiResponseHelper.sendResponse(res, 400, false, 'gcm_api_key is required.');
        return;
    }
    if (req.body.welcome_notification.enabled) {
        if (!req.body.welcome_notification.title) {
            apiResponseHelper.sendResponse(res, 400, false, 'welcome notification title is required.');
            return;
        }
        if (!req.body.welcome_notification.message) {
            apiResponseHelper.sendResponse(res, 400, false, 'welcome notification message is required.');
            return;
        }
        if (!req.body.welcome_notification.url) {
            apiResponseHelper.sendResponse(res, 400, false, 'welcome notification target url is required.');
            return;
        }
    }
    req.body.workspace_id = req.params.workspace_id;
    Application.findOne({'workspace_id': req.params.workspace_id, 'website.url': req.body.website.url, 'website.protocol': req.body.website.protocol}, function(err, application) {
        if (err) {
            var mongooseError = utilsHelper.getMongooseError(err);
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
        }
        else {
            if (application) {
                apiResponseHelper.sendResponse(res, 400, false, 'application with website - ' + req.body.website.protocol + '://' + req.body.website.url + ' already exists on your instance.');
            }
            else {
                new Application(req.body).save(function(err, application) {
                    if (err) {
                        var mongooseError = utilsHelper.getMongooseError(err);
                        apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
                    }
                    else {
                        apiResponseHelper.sendResponse(res, 201, true, 'application created successfully.', application);
                    }
                });
            }
        }
    });
};
/*END: create */

/*START: delete */
applicationController.delete = function(req, res, next) {

    if (!utilsHelper.isValidObjectId(req.params.workspace_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid workspace id.');
        return;
    }
    if (!utilsHelper.isValidObjectId(req.params.application_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid application id.');
        return;
    }
    Application.findOne({'workspace_id': req.params.workspace_id, '_id': req.params.application_id}, function(err, application) {
        if (err) {
            var mongooseError = utilsHelper.getMongooseError(err);
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
        }
        else {
            if (application) {
                Subscription.remove({application_id: application._id}, function(err, deletedSubscriptionsCount) {
                    if (err) {
                        var mongooseError = utilsHelper.getMongooseError(err);
                        apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
                    }
                    else {
                        Notification.remove({application_id: application._id}, function(err, deletedNotificationsCount) {
                            if (err) {
                                var mongooseError = utilsHelper.getMongooseError(err);
                                apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
                            }
                            else {
                                Segment.remove({application_id: application._id}, function(err, deletedNotificationsCount) {
                                    if (err) {
                                        var mongooseError = utilsHelper.getMongooseError(err);
                                        apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
                                    }
                                    else {
                                        Application.remove({_id: application._id}, function(err) {
                                            if (err) {
                                                var mongooseError = utilsHelper.getMongooseError(err);
                                                apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
                                            }
                                            else {
                                                apiResponseHelper.sendResponse(res, 200, true, "Application deleted successfully.");
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
            else {
                apiResponseHelper.sendResponse(res, 400, false, "application not found.");
            }
        }
    });
};
/*END: delete */

/*START: edit */
applicationController.edit = function(req, res, next) {

    if (!utilsHelper.isValidObjectId(req.params.workspace_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid workspace id.');
        return;
    }
    if (!utilsHelper.isValidObjectId(req.params.application_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid application id.');
        return;
    }
    if (!req.body.name) {
        apiResponseHelper.sendResponse(res, 400, false, 'application name is required.');
        return;
    }
    if (!req.body.domain) {
        apiResponseHelper.sendResponse(res, 400, false, 'application domain is required.');
        return;
    }
    if (!req.body.website) {
        apiResponseHelper.sendResponse(res, 400, false, 'website information is required.');
        return;
    }
    if (!req.body.website.protocol) {
        apiResponseHelper.sendResponse(res, 400, false, 'website protocol is required.');
        return;
    }
    if (!req.body.website.url) {
        apiResponseHelper.sendResponse(res, 400, false, 'website url is required.');
        return;
    }
    if (!req.body.gcm_sender_id) {
        apiResponseHelper.sendResponse(res, 400, false, 'gcm_sender_id is required.');
        return;
    }
    if (!req.body.gcm_api_key) {
        apiResponseHelper.sendResponse(res, 400, false, 'gcm_api_key is required.');
        return;
    }
    if (req.body.welcome_notification.enabled) {
        if (!req.body.welcome_notification.title) {
            apiResponseHelper.sendResponse(res, 400, false, 'welcome notification title is required.');
            return;
        }
        if (!req.body.welcome_notification.message) {
            apiResponseHelper.sendResponse(res, 400, false, 'welcome notification message is required.');
            return;
        }
        if (!req.body.welcome_notification.url) {
            apiResponseHelper.sendResponse(res, 400, false, 'welcome notification target url is required.');
            return;
        }
    }
    Application.findOne({'workspace_id': req.params.workspace_id, '_id': req.params.application_id}, function(err, application) {
        if (err) {
            var mongooseError = utilsHelper.getMongooseError(err);
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
        }
        else {
            if (application) {
                application.name = req.body.name;
                application.logo = req.body.logo;
                application.website.protocol = req.body.website.protocol;
                application.website.url = req.body.website.url;
                application.welcome_notification.enabled = req.body.welcome_notification.enabled;
                application.welcome_notification.title = req.body.welcome_notification.title;
                application.welcome_notification.message = req.body.welcome_notification.message;
                application.welcome_notification.url = req.body.welcome_notification.url;
                application.save(function(err, application) {
                    if (err) {
                        var mongooseError = utilsHelper.getMongooseError(err);
                        apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
                    }
                    else {
                        apiResponseHelper.sendResponse(res, 200, true, 'updated successfully.', application);
                    }
                });
            }
            else {
                apiResponseHelper.sendResponse(res, 400, false, 'application not found.');
            }
        }
    });
};
/*START: edit */

/*START: get */
applicationController.get = function(req, res, next) {

    if (!utilsHelper.isValidObjectId(req.params.workspace_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid workspace id.');
        return;
    }
    Application.find({'workspace_id': req.params.workspace_id}, function(err, applications) {
        if (err) {
            var mongooseError = utilsHelper.getMongooseError(err);
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
        }
        else {
           apiResponseHelper.sendResponse(res, 200, true, 'list of applications.', applications);
        }
    });
};
/*END: get */

/*START: getApplication */
applicationController.getApplication = function(req, res, next) {

    if (!utilsHelper.isValidObjectId(req.params.workspace_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid workspace id.');
        return;
    }
    if (!utilsHelper.isValidObjectId(req.params.application_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid application id.');
        return;
    }
    Application.findOne({'workspace_id': req.params.workspace_id, '_id': req.params.application_id}, function(err, application) {
        if (err) {
            var mongooseError = utilsHelper.getMongooseError(err);
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
        }
        else {
            if (application) {
                apiResponseHelper.sendResponse(res, 200, true, 'application information', application);
            }
            else {
                piResponseHelper.sendResponse(res, 400, true, 'application not found');
            }
        }
    });
};
/*END: getApplication */

module.exports = applicationController;