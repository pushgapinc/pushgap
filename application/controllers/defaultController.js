/*START: module includes*/
var defaultController = {};
var path = require('path');
var config = require(path.join(__dirname, '..', 'config', 'config'));
var utilsHelper = require(path.join(__dirname, '..', 'helpers', 'utilsHelper'));
/*END: module includes*/

/*START: action*/
defaultController.action = function(req, res, next) {
    var protocol = req.protocol;
    var host = protocol + '://' + req.headers.host;
    var domain = req.headers.host;
    var api = protocol + '://' + 'api.' + domain;
    res.render('actions/index', {host: host, api: api, appName: config.app.name, protocol: protocol, domain: domain});
};
/*END: action*/

module.exports = defaultController;