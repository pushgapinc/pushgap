/*START: module includes*/
var inviteController = {};
var path = require('path');
var crypto = require('crypto');
var nodeUuid = require('node-uuid');
var emailHelper = require(path.join(__dirname, '..', 'helpers', 'emailHelper'));
var apiResponseHelper = require(path.join(__dirname, '..', 'helpers', 'apiResponseHelper'));
var utilsHelper = require(path.join(__dirname, '..', 'helpers', 'utilsHelper'));
var Invite = require(path.join(__dirname, '..', 'models', 'invite'));
var User = require(path.join(__dirname, '..', 'models', 'user'));
var $q = require('q');
/*END: module includes*/

/*START: createInvite */
inviteController.createInvite = function(req, res, next) {
    if (!req.body.email) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid user email.');
        return;
    }
    req.body.workspace_id = req.params.workspace_id;
    req.body.invite_key = crypto.createHash('sha256').update(nodeUuid.v4()).update(crypto.randomBytes(256)).digest('hex');


    User.findOne({username: req.body.email, workspace_id: req.body.workspace_id}, function(err, user)
    {
        if (err)
        {
            var mongooseError = utilsHelper.getMongooseError(err);
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
        }
        else
        {
            if (user) {
                apiResponseHelper.sendResponse(res, 400, false, 'user already exists in your workspace.cannot send an invite.');
            }
            else {
                Invite.findOne({email: req.body.email, workspace_id: req.body.workspace_id}, function(err, existingInvite) {
                    if (err) {
                        var mongooseError = utilsHelper.getMongooseError(err);
                        apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
                    }
                    else {
                        if (existingInvite) {
                            apiResponseHelper.sendResponse(res, 400, false, 'invite already exists and is pending from the user.cannot send an invite again.');
                        }
                        else {
                            new Invite(req.body).save(function(err, invite) {
                                if (err) {
                                    var mongooseError = utilsHelper.getMongooseError(err);
                                    apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
                                }
                                else {
                                    apiResponseHelper.sendResponse(res, 200, true, 'invite sent successfully.');
                                    var email = {};
                                    email.to = invite.email;
                                    email.from = 'invite@pushgap.com';
                                    email.subject = 'invite from pushGap';
                                    email.html = 'You have been invited to pushgap portal. Here is your registration link - <a href="' + req.protocol + '://' + req.headers.host.replace('api.', req.headers.workspace + '.') + '/#invite?key=' + invite.invite_key + '&email=' + invite.email + '">here</a>';
                                    emailHelper.sendMail(email);
                                }
                            });
                        }
                    }
                });
            }
        }
    });
};
/*END: createInvite */

/*START: acceptInvite */
inviteController.acceptInvite = function(req, res, next) {
    if (!req.query.key) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid invite key.');
        return;
    }

    Invite.findOne({email: req.body.email, workspace_id: req.params.workspace_id, invite_key: req.query.key}, function(err, invite) {
        if (err) {
            var mongooseError = utilsHelper.getMongooseError(err);
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
        }
        else {
            if (invite) {
                User.findOne({workspace_id: req.params.workspace_id, username: req.body.email}, function(err, existingUser) {
                    if (err) {
                        var mongooseError = utilsHelper.getMongooseError(err);
                        apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
                    }
                    else {
                        if (existingUser) {
                            apiResponseHelper.sendResponse(res, 400, false, "account already exists.");
                        }
                        else {
                            var user = {};
                            user.username = req.body.email;
                            user.password = utilsHelper.generateHash(req.body.password);
                            user.workspace_id = req.params.workspace_id;
                            user.role = 'admin';
                            user.active = true;

                            new User(user).save(function(err, user) {
                                if (err) {
                                    var mongooseError = utilsHelper.getMongooseError(err);
                                    apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
                                }
                                else {
                                    apiResponseHelper.sendResponse(res, 200, true, "Account created successfully.");
                                    invite.remove();
                                }
                            });
                        }
                    }
                });
            }
            else {
                apiResponseHelper.sendResponse(res, 400, false, 'invitation not found.');
            }
        }
    });
};
/*END: acceptInvite */

/*START: getInvites */
inviteController.getInvites = function(req, res, next) {
    Invite.find({workspace_id: req.params.workspace_id},'_id email created_at', function(err, invites) {
        if (err) {
            var mongooseError = utilsHelper.getMongooseError(err);
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
        }
        else {
            apiResponseHelper.sendResponse(res, 200, true, 'invites list', invites);
        }
    });
};
/*END: getInvites */

/*START: deleteInvite */
inviteController.deleteInvite = function(req, res, next) {
    Invite.remove({workspace_id: req.params.workspace_id, _id: req.params.invite_id}, function(err, deleteCount) {
        if (err) {
            var mongooseError = utilsHelper.getMongooseError(err);
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
        }
        else {
            apiResponseHelper.sendResponse(res, 200, true, 'invite deleted successfully.');
        }
    });
};
/*END: deleteInvite */

/*START: resendInvite */
inviteController.resendInvite = function(req, res, next) {
    Invite.findOne({workspace_id: req.params.workspace_id, _id: req.params.invite_id}, function(err, invite) {
        if (err) {
            var mongooseError = utilsHelper.getMongooseError(err);
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
        }
        else {
            if (invite) {
                var email = {};
                email.to = invite.email;
                email.from = 'invite@pushgap.com';
                email.subject = 'invite from pushGap';
                email.html = 'You have been invited to pushgap portal. Here is your registration link - <a href="' + req.protocol + '://' + req.headers.host.replace('api.', req.headers.workspace + '.') + '/#invite?key=' + invite.invite_key + '&email=' + invite.email + '">here</a>';
                emailHelper.sendMail(email);
                apiResponseHelper.sendResponse(res, 200, true, 'invite sent successfully.');
            }
            else {
                apiResponseHelper.sendResponse(res, 400, false, 'invite not found.');
            }
        }
    });
};
/*END: resendInvite */

module.exports = inviteController;