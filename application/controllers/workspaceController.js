/*START: module includes*/
var workspaceController = {};
var path = require('path');
var crypto = require('crypto');
var nodeUuid = require('node-uuid');
var config = require(path.join(__dirname, '..', 'config', 'config'));
var apiResponseHelper = require(path.join(__dirname, '..', 'helpers', 'apiResponseHelper'));
var emailHelper = require(path.join(__dirname, '..', 'helpers', 'emailHelper'));
var utilsHelper = require(path.join(__dirname, '..', 'helpers', 'utilsHelper'));
var Workspace = require(path.join(__dirname, '..', 'models', 'workspace'));
var userController = require(path.join(__dirname, 'userController'));
var $q = require('q');
/*END: module includes*/

/*START: create */
workspaceController.create = function(req, res, next) {

    if (!req.body.name) {
        apiResponseHelper.sendResponse(res, 400, false, 'workspace name is required');
        return;
    }
    if (!req.body.username) {
        apiResponseHelper.sendResponse(res, 400, false, 'username(email) is required');
        return;
    }
    if (!req.body.password) {
        apiResponseHelper.sendResponse(res, 400, false, 'password is required');
        return;
    }
    if (utilsHelper.containsSpecialChars(req.body.name)) {
        apiResponseHelper.sendResponse(res, 400, false, 'special characters are not allowed in the workspace name.');
        return;
    }
    if (config.disallowedSubdomains.indexOf(req.body.name) != -1) {
        apiResponseHelper.sendResponse(res, 400, false, 'This workspace name is not allowed.');
        return;
    }
    req.body.api_secret = crypto.createHash('sha256').update(nodeUuid.v4()).update(crypto.randomBytes(256)).digest('hex');
    workspaceController._create_({name: req.body.name, api_secret: req.body.api_secret}).then(function(workspace) {
        userController.createUser(req.body.username, req.body.password, workspace._id, 'owner', utilsHelper.generateRandomKey(true)).then(function(user) {
            apiResponseHelper.sendResponse(res, 201, true, 'Account created successfully.', {workspace_id: workspace._id});
            var email = {};
            email.to = user.username;
            email.from = 'welcome@pushgap.com';
            email.subject = 'Welcome to PushGap';
            email.html = 'Please confirm your email id. Here is your confirmation link - <a href="' + req.protocol + '://' + req.headers.host.replace('api.', '') + '/action#emailconfirmation/' + user.activation_key + '">here</a>';
            emailHelper.sendMail(email);
        }, function(mongooseError) {
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
            Workspace.find({_id: workspace._id}).remove().exec();
        });
    }, function(mongooseError) {
        apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
    });
};
/*END: create */

/*START: _create_ */
workspaceController._create_ = function(workspace) {
    var deferred = $q.defer();
    new Workspace(workspace).save(function(err, workspace) {
        if (err) {
            deferred.reject(utilsHelper.getMongooseError(err));
        }
        else {
            deferred.resolve(workspace);
        }
    });
    return deferred.promise;
};
/*END: _create_ */

/*START: activate */
workspaceController.activate = function(req, res, next) {
   if (!req.query.key) {
       apiResponseHelper.sendResponse(res, 400, false, 'invalid registration key');
       return;
   }

    userController.findOne({activation_key: req.query.key}).then(function(user) {
        if (user) {
            if (user.active) {
                apiResponseHelper.sendResponse(res, 200, true, 'workspace has been already activated');
            }
            else {
                user.update({active: true}, function(err, updatedUser) {
                    if (err) {
                        var mongooseError = utilsHelper.getMongooseError(err);
                        apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
                    }
                    else {
                        apiResponseHelper.sendResponse(res, 200, true, 'workspace has been activated successfully');
                    }
                });
            }
        }
        else {
            apiResponseHelper.sendResponse(res, 400, false, 'invalid registration key');
        }
    }, function(mongooseError) {
        apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
    });
};
/*END: activate */

/*START: authorizeWorkspaceAccess*/
workspaceController.authorizeWorkspaceAccess = function(req, res, next) {
    if (req.params.workspace_id == req.key.workspace_id) {
        next();
    }
    else {
        apiResponseHelper.sendResponse(res, 401, false, 'unauthorized workspace access');
    }
};
/*END: authorizeWorkspaceAccess*/

/*START: getApiSecret*/
workspaceController.getApiSecret = function(req, res, next) {
    Workspace.findOne({_id: req.params.workspace_id}, function(err, workspace) {
        if (err) {
            var mongooseError = utilsHelper.getMongooseError(err);
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
        }
        else {
            if (workspace) {
                apiResponseHelper.sendResponse(res, 200, true, 'api secret', workspace.api_secret);
            }
            else {
                apiResponseHelper.sendResponse(res, 400, false, 'workspace not found');
            }
        }
    });
};
/*END: getApiSecret*/

module.exports = workspaceController;