/*START: module includes*/
var authController = {};
var path = require('path');
var jwt = require('jsonwebtoken');
var config = require(path.join(__dirname, '..', 'config', 'config'));
var apiResponseHelper = require(path.join(__dirname, '..', 'helpers', 'apiResponseHelper'));
var utilsHelper = require(path.join(__dirname, '..', 'helpers', 'utilsHelper'));
var userController = require(path.join(__dirname, 'userController'));
var User = require(path.join(__dirname, '..', 'models', 'user'));
/*END: module includes*/

/*START: login */
authController.login = function(req, res, next) {

    if (!req.body.workspace_id) {
        apiResponseHelper.sendResponse(res, 400, false, 'workspace id is required.');
        return;
    }
    if (!req.body.username) {
        apiResponseHelper.sendResponse(res, 400, false, 'username(email) is required.');
        return;
    }
    if (!req.body.password) {
        apiResponseHelper.sendResponse(res, 400, false, 'password is required.');
        return;
    }
    if (!utilsHelper.isValidObjectId(req.body.workspace_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid workspace id.');
        return;
    }

    User.findOne({username: req.body.username, workspace_id: req.body.workspace_id}).populate('workspace_id').exec(function(err, user) {
        if (err) {
            var mongooseError = utilsHelper.getMongooseError(err);
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
        }
        else {
            if (user) {
                if (!user.active) {
                    apiResponseHelper.sendResponse(res, 400, false, 'user login disabled.');
                    return;
                }
                if (user.validatePassword(req.body.password)) {
                    //user payload in token
                    var userPayload = {};
                    userPayload.user_id = user.get('_id');
                    userPayload.username = user.get('username');
                    userPayload.role = user.get('role');
                    userPayload.workspace_id = user.get('workspace_id')._id;
                    userPayload.subscribed_plan = user.get('workspace_id').subscribed_plan;
                    jwt.sign(userPayload, config.jwt.secret, config.jwt.options, function(err, authToken) {
                        apiResponseHelper.sendResponse(res, 200, true, 'login successful', authToken);
                    });
                }
                else {
                    apiResponseHelper.sendResponse(res, 400, false, 'invalid credentials.');
                }
            }
            else {
                apiResponseHelper.sendResponse(res, 400, false, 'invalid credentials.');
            }
        }
    });
};
/*END: login */

/*START: authenticateToken*/
authController.authenticateToken = function(req, res, next) {
    var token = req.headers.authorization;
    if (!token) {
        var errorPayload = {};
        errorPayload.message = 'authorization header is required.';
        errorPayload.code = 'logout';
        apiResponseHelper.sendResponse(res, 401, false, 'unauthorized access.', null, errorPayload);
        return;
    }

    jwt.verify(token, config.jwt.secret, function (err, decoded)
    {
        if (err)
        {
            err.code = 'logout';
            apiResponseHelper.sendResponse(res, 401, false, 'unauthorized access.', null, err);

        }
        else
        {
            req.key = decoded;
            next();
        }
    });
};
/*END: authenticateToken*/

module.exports = authController;