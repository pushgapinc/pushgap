/*START: module includes*/
var notificationController = {};
var path = require('path');
var webPush = require('web-push');
var $q = require('q');
var config = require(path.join(__dirname, '..', 'config', 'config'));
var winston = require(path.join(__dirname, '..', 'utils', 'winston'));
var apiResponseHelper = require(path.join(__dirname, '..', 'helpers', 'apiResponseHelper'));
var utilsHelper = require(path.join(__dirname, '..', 'helpers', 'utilsHelper'));
var Subscription = require(path.join(__dirname, '..', 'models', 'subscription'));
var Notification = require(path.join(__dirname, '..', 'models', 'notification'));
var Application = require(path.join(__dirname, '..', 'models', 'application'));
/*END: module includes*/

/*START: create */
notificationController.create = function(req, res, next) {
    if (!utilsHelper.isValidObjectId(req.params.workspace_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid workspace id.');
        return;
    }
    if (!utilsHelper.isValidObjectId(req.params.application_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid application id.');
        return;
    }
    if (!req.body.title) {
        apiResponseHelper.sendResponse(res, 400, false, 'notification title is required.');
        return;
    }
    if (!req.body.message) {
        apiResponseHelper.sendResponse(res, 400, false, 'notification message is required.');
        return;
    }
    if (!req.body.icon) {
        apiResponseHelper.sendResponse(res, 400, false, 'notification icon is required.');
        return;
    }
    req.body.workspace_id = req.params.workspace_id;
    req.body.application_id = req.params.application_id;

    notificationController._create_(req.body).then(function(notification) {
        notificationController.sendNotifications(notification).then(function() {
            apiResponseHelper.sendResponse(res, 200, true);
        }, function(errorMessage) {
            apiResponseHelper.sendResponse(res, 400, false, errorMessage);
        });
    }, function(mongooseError) {
        apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
    });
};
/*END: create */

/*START: get */
notificationController.get = function(req, res, next) {
    if (!utilsHelper.isValidObjectId(req.params.workspace_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid workspace id.');
        return;
    }
    if (!utilsHelper.isValidObjectId(req.params.application_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid application id.');
        return;
    }
    var options = {};
    options.sort = {};
    options.sort.created_at = -1;
    options.limit = 10;
    if (!req.query.page) {
        req.query.page = 1;
    }
    req.query.page -= 1;
    options.skip = req.query.page * options.limit;
    Notification.find({'workspace_id': req.params.workspace_id, 'application_id': req.params.application_id},{}, options, function(err, notifications) {
        if (err) {
            var mongooseError = utilsHelper.getMongooseError(err);
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
        }
        else {
            apiResponseHelper.sendResponse(res, 200, true, 'list of notifications.', notifications);
        }
    });
};
/*END: get */

/*START: _create_*/
notificationController._create_ = function(notification) {
    var deferred = $q.defer();
    new Notification(notification).save(function(err, notification) {
        if (err) {
            deferred.reject(utilsHelper.getMongooseError(err));
        }
        else {
            deferred.resolve(notification);
        }
    });
    return deferred.promise;
};
/*END: _create_*/

/*START: sendNotifications*/
notificationController.sendNotifications = function(notification) {

    var deferred = $q.defer();

    Application.findOne({'_id': notification.application_id}, function(err, application) {
        if (application) {
            var notificationPayload = {};
            notificationPayload.id = notification.get('_id');
            notificationPayload.title = notification.get('title');
            notificationPayload.body = notification.get('message');
            if (notification.get('icon')) {
                notificationPayload.logo = notification.get('icon');
            }
            else {
                notificationPayload.logo = application.get('logo');
            }
            notificationPayload.targetLocation = notification.get('target_location');
            notificationPayload = JSON.stringify(notificationPayload);

            var query = {};
            query.workspace_id = notification.workspace_id;
            query.application_id = notification.application_id;

            if (notification.segmentation) {
                if (notification.segmentation.type == 'existing_segment' || notification.segmentation.type == 'custom_segment') {
                    var segment = notification.segmentation;
                    if (segment.definition && segment.definition.conditions) {
                        segment.definition.conditions.forEach(function(condition) {
                            if (condition.filter == 'browser') {
                                query.browser = condition.value;
                            }
                            else if (condition.filter == 'is_mobile') {
                                query.is_mobile = condition.value;
                            }
                            else if (condition.filter == 'tags') {
                                if (condition.operator == 'containing_one_of') {
                                    query.$or = [];
                                    var tags = condition.value.split(";");
                                    tags.forEach(function(tag) {
                                        query.$or.push({
                                            'tags': tag
                                        });
                                    });
                                }
                                if (condition.operator == 'containing_all') {
                                    query.$and = [];
                                    var tags = condition.value.split(";");
                                    tags.forEach(function(tag) {
                                        query.$and.push({
                                            'tags': tag
                                        });
                                    });
                                }
                            }
                        });
                    }
                }
            }

            Subscription.find(query, function(err, subscriptions) {
                subscriptions.forEach(function(subscription) {
                    notificationController.pushNotification(subscription.endpoint, notificationPayload, subscription.key, subscription.auth, application.get('gcm_api_key'), notification.get('ttl'));
                });
            });
            deferred.resolve();
        }
        else {
            deferred.reject('application not found.');
        }
    });
    return deferred.promise;
};
/*END: sendNotifications*/

/*START: pushNotification*/
notificationController.pushNotification = function(endpoint, payload, key, auth, gcm_api_key, ttl) {

    var pushSubscription = {
        endpoint: endpoint,
        keys: {
            p256dh: key,
            auth: auth
        }
    };
    var options = {
            gcmAPIKey: gcm_api_key
    };

    if (ttl) {
        options.TTL = ttl;
    }

    webPush.sendNotification(
        pushSubscription,
        payload,
        options
    )
    .then(function(data) {
    }, function(err) {
        winston.info(err);
    });
};
/*END: pushNotification*/

/*START: actions*/
notificationController.actions = function(req, res, next) {
    if (req.params.action_type == 'receive') {
        Notification.update({_id: req.params.notification_id}, {$inc: { receive_count: 1 }}, function(err, updatedCount) {
            if (err) {
                var mongooseError = utilsHelper.getMongooseError(err);
                apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
            }
            else {
                apiResponseHelper.sendResponse(res, 200, true);
            }
        });
        return;
    }
    if (req.params.action_type == 'click') {
        Notification.update({_id: req.params.notification_id}, {$inc: { click_count: 1 }}, function(err, updatedCount) {
            if (err) {
                var mongooseError = utilsHelper.getMongooseError(err);
                apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
            }
            else {
                apiResponseHelper.sendResponse(res, 200, true);
            }
        });
        return;
    }
    apiResponseHelper.sendResponse(res, 400, false, 'invalid action.');
};
/*END: actions*/

/*START: getLatestNotification*/
notificationController.getLatestNotification = function(req, res, next) {
    if (!req.query.application) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid application id.');
        return;
    }
    Application.findOne({domain: req.query.application}, function(err, application) {
        if (err) {
            var mongooseError = utilsHelper.getMongooseError(err);
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
        }
        else {
            if (application) {
                Notification.findOne({application_id: application._id}, {}, { sort: { 'created_at' : -1 } }, function(err, notification) {
                    if (err) {
                        var mongooseError = utilsHelper.getMongooseError(err);
                        apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
                    }
                    else {
                        var notificationPayload = {};
                        notificationPayload.id = notification.get('_id');
                        notificationPayload.title = notification.get('title');
                        notificationPayload.body = notification.get('message');
                        notificationPayload.logo = application.get('logo');
                        notificationPayload.targetLocation = notification.get('target_location');
                        apiResponseHelper.sendResponse(res, 200, true, 'notification content.', notificationPayload);
                    }
                });
            }
            else {
                apiResponseHelper.sendResponse(res, 400, false, 'application not found.');
            }
        }
    });
};
/*END: getLatestNotification*/

/*START: getLatestNotificationPrivate*/
notificationController.getLatestNotificationPrivate = function(req, res, next) {
    Notification.findOne({application_id: req.params.application_id}, {}, { sort: { 'created_at' : -1 } }, function(err, notification) {
        if (err) {
            var mongooseError = utilsHelper.getMongooseError(err);
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
        }
        else {
            apiResponseHelper.sendResponse(res, 200, true, 'notification content.', notification);
        }
    });
};
/*END: getLatestNotificationPrivate*/

module.exports = notificationController;