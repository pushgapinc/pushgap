/*START: module includes*/
var authorizationController = {};
var path = require('path');
var jwt = require('jsonwebtoken');
var config = require(path.join(__dirname, '..', 'config', 'config'));
var apiResponseHelper = require(path.join(__dirname, '..', 'helpers', 'apiResponseHelper'));
var utilsHelper = require(path.join(__dirname, '..', 'helpers', 'utilsHelper'));
/*END: module includes*/

/*START: authorizeOwner */
authorizationController.authorizeOwner = function(req, res, next) {

    if (req.key.role == 'owner') {
        next();
    }
    else {
        apiResponseHelper.sendResponse(res, 401, false, 'unauthorized action');
    }
};
/*END: authorizeOwner */

module.exports = authorizationController;