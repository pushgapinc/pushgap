/*START: module includes*/
var userController = {};
var path = require('path');
var emailHelper = require(path.join(__dirname, '..', 'helpers', 'emailHelper'));
var apiResponseHelper = require(path.join(__dirname, '..', 'helpers', 'apiResponseHelper'));
var utilsHelper = require(path.join(__dirname, '..', 'helpers', 'utilsHelper'));
var User = require(path.join(__dirname, '..', 'models', 'user'));
var $q = require('q');
/*END: module includes*/

/*START: createUser */
userController.createUser = function(username, password, workspace_id, role, activation_key) {
    var deferred = $q.defer();

    var query = {};
    query.username = username;
    query.password = utilsHelper.generateHash(password);
    query.workspace_id = workspace_id;
    query.role = role;
    query.activation_key = activation_key;

    new User(query).save(function(err, user) {
        if (err) {
            deferred.reject(utilsHelper.getMongooseError(err));
        }
        else {
            deferred.resolve(user);
        }
    });
    return deferred.promise;
};
/*END: createUser */

/*START: getUsers */
userController.getUsers = function(req, res, next) {
    User.find({workspace_id: req.params.workspace_id, role: 'admin'},'_id username created_at active role', function(err, users) {
        if (err) {
            var mongooseError = utilsHelper.getMongooseError(err);
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
        }
        else {
            apiResponseHelper.sendResponse(res, 200, true, 'users list', users);
        }
    });
};
/*END: getUsers */

/*START: deleteUser */
userController.deleteUser = function(req, res, next) {
    User.remove({workspace_id: req.params.workspace_id, _id: req.params.user_id}, function(err, deleteCount) {
        if (err) {
            var mongooseError = utilsHelper.getMongooseError(err);
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
        }
        else {
            apiResponseHelper.sendResponse(res, 200, true, 'user deleted successfully.');
        }
    });
};
/*END: deleteUser */

/*START: findOne*/
userController.findOne = function(query) {
    var deferred = $q.defer();

    User.findOne(query, function(err, user) {
        if (err) {
            deferred.reject(utilsHelper.getMongooseError(err));
        }
        else {
            deferred.resolve(user);
        }
    });
    return deferred.promise;
};
/*END: findOne*/

/*START: requestPasswordReset*/
userController.requestPasswordReset = function(req, res, next) {
    if (!req.query.username) {
        apiResponseHelper.sendResponse(res, 400, false, 'username is required.');
        return;
    }
    if (!utilsHelper.isValidObjectId(req.params.workspace_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid workspace id.');
        return;
    }
    User.findOne({workspace_id: req.params.workspace_id, username: req.query.username}, function(err, user) {
        if (err) {
            var mongooseError = utilsHelper.getMongooseError(err);
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
        }
        else {
            if (user) {
                if (!user.active) {
                    apiResponseHelper.sendResponse(res, 400, false, 'unable to reset password as the user is not' +
                        ' active.');

                }
                else {
                    var expiry_date = new Date();
                    expiry_date.setDate(expiry_date.getDate() + 1);
                    user.set('reset_password.key', utilsHelper.generateRandomKey(true));
                    user.set('reset_password.expires_on', expiry_date);
                    user.save(function(err, user) {
                        if (err) {
                            var mongooseError = utilsHelper.getMongooseError(err);
                            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
                        }
                        else {
                            apiResponseHelper.sendResponse(res, 201, true, 'request initiated. please check your' +
                                ' mailbox.');
                            var email = {};
                            email.to = user.username;
                            email.from = 'resetpassword@pushgap.com';
                            email.subject = 'Reset Your Password';
                            email.html = 'Here is your reset password link - <a href="' + req.protocol + '://' + req.headers.host.replace('api.', req.headers.workspace + '.') + '/#resetpassword?key=' + user.reset_password.key + '">here</a>';
                            emailHelper.sendMail(email);


                        }
                    });
                }
            }
            else {
                apiResponseHelper.sendResponse(res, 400, false, 'user not found.');

            }
        }
    });
};
/*END: requestPasswordReset*/

/*START: changePassword*/
userController.changePassword = function(req, res, next)
{
    if (!req.query.key) {
        apiResponseHelper.sendResponse(res, 400, false, 'password reset key is required.');
        return;
    }
    if (!req.body.newPassword) {
        apiResponseHelper.sendResponse(res, 400, false, 'new password is required.');
        return;
    }
    User.findOne({'reset_password.key': req.query.key}, function(err, user) {
        if (err) {
            var mongooseError = utilsHelper.getMongooseError(err);
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
        }
        else {
            if (user) {
                if (new Date() > user.get('reset_password.expires_on')) {
                    apiResponseHelper.sendResponse(res, 400, false, 'password reset key expired.');

                }
                else {
                    user.set('password', utilsHelper.generateHash(req.body.newPassword));
                    user.set('reset_password.key', undefined);
                    user.set('reset_password.expires_on', undefined);
                    user.save(function(err, user) {
                        if (err) {
                            var mongooseError = utilsHelper.getMongooseError(err);
                            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
                        }
                        else {
                            apiResponseHelper.sendResponse(res, 200, true, 'password changed successfully');
                        }
                    });
                }
            }
            else {
                apiResponseHelper.sendResponse(res, 400, false, 'invalid password reset key.');

            }
        }
    });
};
/*END: changePassword*/

module.exports = userController;