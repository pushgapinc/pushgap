/*START: module includes*/
var subscriptionController = {};
var path = require('path');
var config = require(path.join(__dirname, '..', 'config', 'config'));
var apiResponseHelper = require(path.join(__dirname, '..', 'helpers', 'apiResponseHelper'));
var utilsHelper = require(path.join(__dirname, '..', 'helpers', 'utilsHelper'));
var Subscription = require(path.join(__dirname, '..', 'models', 'subscription'));
var Application = require(path.join(__dirname, '..', 'models', 'application'));
var notificationController = require(path.join(__dirname, 'notificationController'));
/*END: module includes*/

/*START: subscribe */
subscriptionController.subscribe = function(req, res, next) {
    if (!req.query.workspace_id) {
        apiResponseHelper.sendResponse(res, 400, false, 'workspace id is required.');
        return;
    }
    if (!req.query.application_id) {
        apiResponseHelper.sendResponse(res, 400, false, 'application id is required.');
        return;
    }
    if (!req.body.endpoint) {
        apiResponseHelper.sendResponse(res, 400, false, 'endpoint is required.');
        return;
    }
    if (!req.body.subscription_id) {
        apiResponseHelper.sendResponse(res, 400, false, 'subscription_id is required.');
        return;
    }
    if (!req.body.browser) {
        apiResponseHelper.sendResponse(res, 400, false, 'browser name is required.');
        return;
    }
    if (req.body.is_mobile == 'undefined' || req.body.is_mobile == null) {
        req.body.is_mobile = false;
    }

    req.body.workspace_id = req.query.workspace_id;
    req.body.application_id = req.query.application_id;

    Subscription.findOne({workspace_id: req.body.workspace_id, application_id: req.body.application_id, subscription_id: req.body.subscription_id}, function(err, subscription) {
        if (subscription) {
            subscription.endpoint = req.body.endpoint;
            subscription.browser = req.body.browser;
            subscription.is_mobile = req.body.is_mobile;
            subscription.key = req.body.key;
            subscription.auth = req.body.auth;
        }
        else {
            subscription = new Subscription(req.body);
        }
        subscription.save(function(err, subscription) {
            if (err) {
                var mongooseError = utilsHelper.getMongooseError(err);
                apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
            }
            else {
                apiResponseHelper.sendResponse(res, 201, true, 'subscribed successfully.', subscription);
                Application.findOne({'workspace_id': req.query.workspace_id, '_id': req.query.application_id}, function(err, application) {
                    if (err) {
                        var mongooseError = utilsHelper.getMongooseError(err);
                        apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
                    }
                    else {
                        if (application.get('welcome_notification.enabled')) {
                            var title = application.get('welcome_notification.title');
                            var message = application.get('welcome_notification.message');
                            var notificationPayload = {};
                            notificationPayload.title = title ? title : 'Welcome';
                            notificationPayload.body = message ? message : 'Thanks for Subscribing to us.';
                            notificationPayload.TTL = 1000;
                            notificationPayload.logo = application.get('logo');
                            if (application.get('welcome_notification.url')) {
                                notificationPayload.targetLocation = application.get('welcome_notification.url');
                            }
                            else {
                                notificationPayload.targetLocation = application.get('website.protocol') + '://' + application.get('website.url');
                            }
                            notificationPayload = JSON.stringify(notificationPayload);
                            notificationController.pushNotification(subscription.endpoint,notificationPayload, subscription.key, subscription.auth, application.get('gcm_api_key'));
                        }
                    }
                });
            }
        });
    });
};
/*END: subscribe */

/*START: getSubscriptions */
subscriptionController.getSubscriptions = function(req, res, next) {

    if (!utilsHelper.isValidObjectId(req.params.workspace_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid workspace id.');
        return;
    }
    if (!utilsHelper.isValidObjectId(req.params.application_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid application id.');
        return;
    }
    Subscription.find({'workspace_id': req.params.workspace_id, 'application_id': req.params.application_id}, function(err, subscriptions) {
        if (err) {
            var mongooseError = utilsHelper.getMongooseError(err);
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
        }
        else {
            apiResponseHelper.sendResponse(res, 200, true, 'subscriptions', subscriptions);
        }
    });
};
/*END: getSubscriptions */

/*START: getSubscriptionsCount */
subscriptionController.getSubscriptionsCount = function(req, res, next) {

    if (!utilsHelper.isValidObjectId(req.params.workspace_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid workspace id.');
        return;
    }
    if (!utilsHelper.isValidObjectId(req.params.application_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid application id.');
        return;
    }

    Subscription.count({'workspace_id': req.params.workspace_id, 'application_id': req.params.application_id}, function(err, totalSubscriptions) {
        Subscription.count({'workspace_id': req.params.workspace_id, 'application_id': req.params.application_id, 'browser': 'chrome'}, function(err, chromeSubscriptions) {
            Subscription.count({'workspace_id': req.params.workspace_id, 'application_id': req.params.application_id, 'browser': 'firefox'}, function(err, firefoxSubscriptions) {
                var subscriptions = {};
                subscriptions.totalSubscriptions = totalSubscriptions;
                subscriptions.chromeSubscriptions = chromeSubscriptions;
                subscriptions.firefoxSubscriptions = firefoxSubscriptions;
                apiResponseHelper.sendResponse(res, 200, true, 'subscriptions count', subscriptions);
            });
        });
    });
};
/*END: getSubscriptionsCount */

/*START: renderSubscriptionPage */
subscriptionController.renderSubscriptionPage = function(req, res, next) {
    if (req.subdomains.length == 0 || req.subdomains.length > 1) {
        next();
    }
    else {
        Application.findOne({domain: req.subdomains[0]}, function(err, application) {
            if (application) {
                var protocol = req.protocol;
                var host = protocol + '://' + req.headers.host;
                var domain = req.headers.host.substring(req.headers.host.indexOf(".") + 1);
                var api = protocol + '://' + 'api.' + domain;
                var website = application.get('website.protocol') + '://' + application.get('website.url');
                res.render('application/subscribe', {workspaceId: application.workspace_id,applicationId: application._id,  host: host, api: api, protocol: protocol, domain: domain, logo: application.logo, website: website});
            }
            else {
                next();
            }
        });
    }
};
/*END: renderSubscriptionPage */

/*START: addTag */
subscriptionController.addTag = function(req, res, next) {

    if (!utilsHelper.isValidObjectId(req.params.application_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid application id.');
        return;
    }
    if (!utilsHelper.isValidObjectId(req.params.subscription_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid subscription id.');
        return;
    }
    Subscription.update({'_id': req.params.subscription_id, 'application_id': req.params.application_id}, {$addToSet: {tags: req.body.name}}, { runValidators: true}, function(err, subscriptionCount) {
        if (err) {
            var mongooseError = utilsHelper.getMongooseError(err);
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
        }
        else {
            if (subscriptionCount.n) {
                apiResponseHelper.sendResponse(res, 200, true, 'tag added successfully.');
            }
            else {
                apiResponseHelper.sendResponse(res, 400, false, 'subscription not found.');
            }
        }
    });
};
/*END: addTag */

/*START: deleteTag */
subscriptionController.deleteTag = function(req, res, next) {

    if (!utilsHelper.isValidObjectId(req.params.application_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid application id.');
        return;
    }
    if (!utilsHelper.isValidObjectId(req.params.subscription_id)) {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid subscription id.');
        return;
    }
    Subscription.update({'_id': req.params.subscription_id, 'application_id': req.params.application_id}, {$pull: {tags: req.body.name}}, function(err, subscriptionCount) {
        if (err) {
            var mongooseError = utilsHelper.getMongooseError(err);
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
        }
        else {
            if (subscriptionCount) {
                apiResponseHelper.sendResponse(res, 200, true, 'tag deleted successfully.');
            }
            else {
                apiResponseHelper.sendResponse(res, 400, false, 'subscription not found.');
            }
        }
    });
};
/*END: deleteTag */

module.exports = subscriptionController;