/*START: module includes*/
var portalController = {};
var path = require('path');
var config = require(path.join(__dirname, '..', 'config', 'config'));
var utilsHelper = require(path.join(__dirname, '..', 'helpers', 'utilsHelper'));
var Workspace = require(path.join(__dirname, '..', 'models', 'workspace'));
var Application = require(path.join(__dirname, '..', 'models', 'application'));
/*END: module includes*/

/*START: home*/
portalController.home = function(req, res, next) {
    if (req.subdomains.length == 0 || req.subdomains.length > 1) {
        next();
    }
    else {
        Workspace.findOne({name: req.subdomains[0]}, function(err, workspace) {
            if (workspace) {
                var protocol = req.protocol;
                var host = protocol + '://' + req.headers.host;
                var domain = req.headers.host.substring(req.headers.host.indexOf(".") + 1);
                var api = protocol + '://' + 'api.' + domain;
                res.render('instance/index', {workspaceId: workspace._id, workspaceName: workspace.name, host: host, api: api, appName: config.app.name, protocol: protocol, domain: domain, subscribedPlan: workspace.subscribed_plan});
            }
            else {
                next();
            }
        });
    }
};
/*END: home*/

/*START: manifest*/
portalController.manifest = function(req, res, next) {
    if (req.subdomains.length == 0 || req.subdomains.length > 1) {
        next();
    }
    else {
        Application.findOne({domain: req.subdomains[0]}, function(err, application) {
            if (application) {
                res.render('application/manifest', {gcm_sender_id: application.gcm_sender_id, domain: application.domain});
            }
            else {
                next();
            }
        });
    }
};
/*END: manifest*/

/*START: serviceWorker*/
portalController.serviceWorker = function(req, res, next) {
    if (req.subdomains.length == 0 || req.subdomains.length > 1) {
        next();
    }
    else {
        var protocol = req.protocol;
        var host = protocol + '://' + req.headers.host;
        var domain = req.headers.host.substring(req.headers.host.indexOf(".") + 1);
        var api = protocol + '://' + 'api.' + domain;
        var application = req.headers.host.substring(0, req.headers.host.indexOf("."));
        res.type('text/javascript');
        res.render('application/service_worker', {host: host, api: api, protocol: protocol, domain: domain, application: application});
    }
};
/*END: serviceWorker*/

module.exports = portalController;