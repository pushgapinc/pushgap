/*START: module includes*/
var apiRouter = require('express').Router({mergeParams: true});
var v1Router = require('express').Router({mergeParams: true});
var path = require('path');
var apiResponseHelper = require(path.join(__dirname, '..', 'helpers', 'apiResponseHelper'));
var workspaceController = require(path.join(__dirname, '..', 'controllers', 'workspaceController'));
var userController = require(path.join(__dirname, '..', 'controllers', 'userController'));
var authController = require(path.join(__dirname, '..', 'controllers', 'authController'));
var subscriptionController = require(path.join(__dirname, '..', 'controllers', 'subscriptionController'));
var applicationController = require(path.join(__dirname, '..', 'controllers', 'applicationController'));
var notificationController = require(path.join(__dirname, '..', 'controllers', 'notificationController'));
var sessionController = require(path.join(__dirname, '..', 'controllers', 'sessionController'));
var inviteController = require(path.join(__dirname, '..', 'controllers', 'inviteController'));
var authorizationController = require(path.join(__dirname, '..', 'controllers', 'authorizationController'));
var segmentController = require(path.join(__dirname, '..', 'controllers', 'segmentController'));
/*END: module includes*/

/*START: routes*/
apiRouter.use('/v1', v1Router);
v1Router.post('/login', authController.login);

v1Router.post('/workspaces', workspaceController.create);
v1Router.patch('/workspaces/activate', workspaceController.activate);
v1Router.get('/workspaces/:workspace_id/apisecret', authController.authenticateToken, workspaceController.authorizeWorkspaceAccess, workspaceController.getApiSecret);

v1Router.post('/workspaces/:workspace_id/invites', authController.authenticateToken, workspaceController.authorizeWorkspaceAccess, authorizationController.authorizeOwner, inviteController.createInvite);
v1Router.get('/workspaces/:workspace_id/invites', authController.authenticateToken, workspaceController.authorizeWorkspaceAccess, authorizationController.authorizeOwner, inviteController.getInvites);
v1Router.delete('/workspaces/:workspace_id/invites/:invite_id', authController.authenticateToken, workspaceController.authorizeWorkspaceAccess, authorizationController.authorizeOwner, inviteController.deleteInvite);
v1Router.put('/workspaces/:workspace_id/invites/:invite_id', authController.authenticateToken, workspaceController.authorizeWorkspaceAccess, authorizationController.authorizeOwner, inviteController.resendInvite);
v1Router.post('/workspaces/:workspace_id/acceptinvite', inviteController.acceptInvite);

v1Router.put('/users/password', userController.changePassword);
v1Router.post('/workspaces/:workspace_id/users/password/reset', userController.requestPasswordReset);
v1Router.get('/workspaces/:workspace_id/users', authController.authenticateToken, workspaceController.authorizeWorkspaceAccess, authorizationController.authorizeOwner, userController.getUsers);
v1Router.delete('/workspaces/:workspace_id/users/:user_id', authController.authenticateToken, workspaceController.authorizeWorkspaceAccess, authorizationController.authorizeOwner, userController.deleteUser);
v1Router.get('/workspaces/:workspace_id/session/user', authController.authenticateToken, workspaceController.authorizeWorkspaceAccess, sessionController.getCurrentUser);

v1Router.post('/workspaces/:workspace_id/applications', authController.authenticateToken, workspaceController.authorizeWorkspaceAccess, authorizationController.authorizeOwner, applicationController.create);
v1Router.delete('/workspaces/:workspace_id/applications/:application_id', authController.authenticateToken, workspaceController.authorizeWorkspaceAccess, authorizationController.authorizeOwner, applicationController.delete);
v1Router.put('/workspaces/:workspace_id/applications/:application_id', authController.authenticateToken, workspaceController.authorizeWorkspaceAccess, applicationController.edit);
v1Router.get('/workspaces/:workspace_id/applications', authController.authenticateToken, workspaceController.authorizeWorkspaceAccess, applicationController.get);
v1Router.get('/workspaces/:workspace_id/applications/:application_id', authController.authenticateToken, workspaceController.authorizeWorkspaceAccess, applicationController.getApplication);

v1Router.post('/subscriptions', subscriptionController.subscribe);
v1Router.get('/workspaces/:workspace_id/applications/:application_id/subscriptions', authController.authenticateToken, workspaceController.authorizeWorkspaceAccess, subscriptionController.getSubscriptions);
v1Router.get('/workspaces/:workspace_id/applications/:application_id/subscriptions/count', authController.authenticateToken, workspaceController.authorizeWorkspaceAccess, subscriptionController.getSubscriptionsCount);

v1Router.get('/workspaces/:workspace_id/applications/:application_id/segments', authController.authenticateToken, workspaceController.authorizeWorkspaceAccess, segmentController.get);
v1Router.post('/workspaces/:workspace_id/applications/:application_id/segments', authController.authenticateToken, workspaceController.authorizeWorkspaceAccess, segmentController.create);
v1Router.put('/workspaces/:workspace_id/applications/:application_id/segments/:segment_id', authController.authenticateToken, workspaceController.authorizeWorkspaceAccess, segmentController.edit);
v1Router.delete('/workspaces/:workspace_id/applications/:application_id/segments/:segment_id', authController.authenticateToken, workspaceController.authorizeWorkspaceAccess, segmentController.delete);
v1Router.post('/workspaces/:workspace_id/applications/:application_id/segments/count', authController.authenticateToken, workspaceController.authorizeWorkspaceAccess, segmentController.getSegmentSubscriptionCount);

v1Router.post('/applications/:application_id/subscriptions/:subscription_id/tags', subscriptionController.addTag);
v1Router.delete('/applications/:application_id/subscriptions/:subscription_id/tags', subscriptionController.deleteTag);

v1Router.post('/notifications/:notification_id/actions/:action_type', notificationController.actions);
v1Router.get('/notifications/latest', notificationController.getLatestNotification);
v1Router.post('/workspaces/:workspace_id/applications/:application_id/notifications', authController.authenticateToken, workspaceController.authorizeWorkspaceAccess, notificationController.create);
v1Router.get('/workspaces/:workspace_id/applications/:application_id/notifications', authController.authenticateToken, workspaceController.authorizeWorkspaceAccess, notificationController.get);
v1Router.get('/workspaces/:workspace_id/applications/:application_id/notifications/latest', authController.authenticateToken, workspaceController.authorizeWorkspaceAccess, notificationController.getLatestNotificationPrivate);
/*END: routes*/

/*START: api - 404*/
apiRouter.use(function(req, res, next) {
    apiResponseHelper.sendResponse(res, 404, false, "api not found.");
});
/*END: api - 404*/

module.exports = apiRouter;