/*START: module includes*/
var defaultRouter = require('express').Router({mergeParams: true});
var path = require('path');
var config = require(path.join(__dirname, '..', 'config', 'config'));
var apiResponseHelper = require(path.join(__dirname, '..', 'helpers', 'apiResponseHelper'));
var utilsHelper = require(path.join(__dirname, '..', 'helpers', 'utilsHelper'));
var defaultController = require(path.join(__dirname, '..', 'controllers', 'defaultController'));
/*END: module includes*/

/*START: routes*/
defaultRouter.get('/action', defaultController.action);
/*END: routes*/

module.exports = defaultRouter;