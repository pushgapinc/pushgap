/*START: module includes*/
var sdkRouter = require('express').Router({mergeParams: true});
var path = require('path');
var apiResponseHelper = require(path.join(__dirname, '..', 'helpers', 'apiResponseHelper'));
var sdkController = require(path.join(__dirname, '..', 'controllers', 'sdkController'));
/*END: module includes*/

/*START: routes*/
sdkRouter.get('/pushgap_sdk.js', sdkController.pushgap_sdk);
sdkRouter.get('/pushgap_client_sdk.js', sdkController.pushgap_client_sdk);
/*END: routes*/

/*START: sdk - 404*/
sdkRouter.use(function(req, res, next) {
    apiResponseHelper.sendResponse(res, 404, false, "404 not found.");
});
/*END: sdk - 404*/

module.exports = sdkRouter;