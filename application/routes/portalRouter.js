/*START: module includes*/
var portalRouter = require('express').Router({mergeParams: true});
var path = require('path');
var apiResponseHelper = require(path.join(__dirname, '..', 'helpers', 'apiResponseHelper'));
var portalController = require(path.join(__dirname, '..', 'controllers', 'portalController'));
var subscriptionController = require(path.join(__dirname, '..', 'controllers', 'subscriptionController'));
/*END: module includes*/

/*START: routes*/
portalRouter.get('/', portalController.home);
portalRouter.get('/subscribe', subscriptionController.renderSubscriptionPage);
portalRouter.get('/manifest.json', portalController.manifest);
portalRouter.get('/service_worker.js', portalController.serviceWorker);
/*END: routes*/

/*START: subdomain - 404*/
portalRouter.use(function(req, res, next) {
    if (req.subdomains.length == 0) {
        next();
    }
    else {
        apiResponseHelper.sendResponse(res, 404, false, "404 not found.");
    }
});
/*END: subdomain - 404*/

module.exports = portalRouter;