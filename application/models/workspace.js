/*START: module includes*/
var mongoose = require('mongoose');
var schema = mongoose.Schema;
/*END: module includes*/

/*START: schema definition*/
var workspaceSchema = new schema({
    name: {
        required: ['workspace name is required'],
        type: String,
        unique: true
    },
    api_secret: {
        unique: true,
        type: String
    },
    subscribed_plan: {
        required: ['workspace subscribed_plan is required'],
        type: String,
        default: 'trial',
        enum: ['trial', 'basic', 'premium']
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});
/*END: schema definition*/

/*START:instantiate the model*/
var Workspace = mongoose.model('workspace', workspaceSchema);
/*END:instantiate the model*/

/*START: schema validations*/
Workspace.schema.path('name').validate(function(name) {
        if (name.length < 6) {
            return false;
        }
        return true;
    },'workspace name should be more than 6 characters.');

Workspace.schema.path('name').validate(function(name) {
    if (name.length > 30) {
        return false;
    }
    return true;
},'workspace name should be less than 30 characters.');
/*END: schema validations*/

/*START: export schema*/
module.exports = Workspace;
/*END: export schema*/



/*NOTES
*
* 1. use path validation if you want to validate schema every time document saved/updated
* 2. use pre save validation if you want to validate only on save
*
* */