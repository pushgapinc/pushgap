/*START: module includes*/
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
/*END: module includes*/

/*START: schema definition*/
var notificationSchema = new Schema({
    workspace_id: {
        required: ['invalid workspace id.'],
        type: Schema.Types.ObjectId,
        ref: 'workspace'
    },
    application_id: {
        required: ['invalid application id.'],
        type: Schema.Types.ObjectId,
        ref: 'application'
    },
    title: {
        type: String,
        required: ['notification title is required.']
    },
    message: {
        type: String,
        required: ['notification message is required.']
    },
    target_location: {
        type: String
    },
    icon: {
        type: String,
        required: ['notification icon is required.']
    },
    ttl: {
        type: Number
    },
    receive_count: {
        type: Number,
        default: 0
    },
    click_count: {
        type: Number,
        default: 0
    },
    segmentation: {
        type: Schema.Types.Mixed,
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});
/*END: schema definition*/

/*START:instantiate the model*/
var Notification = mongoose.model('notification', notificationSchema);
/*END:instantiate the model*/

/*START: export schema*/
module.exports = Notification;
/*END: export schema*/