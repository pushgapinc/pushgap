/*START: module includes*/
var path = require('path');
var bcrypt = require('bcrypt-nodejs');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
/*END: module includes*/

/*START: schema definition*/
var userSchema = new Schema({
    username: {
        type: String,
        required: ['username is required']
    },
    password: {
        type: String,
        required: ['password is required']
    },
    workspace_id: {
        type: Schema.Types.ObjectId,
        ref: 'workspace',
        required: ['invalid workspace id']
    },
    active: {
        type: Boolean,
        required: true,
        default: false
    },
    activation_key: {
        type: String
    },
    role: {
        type: String,
        required: ['role is required'],
        enum: ['owner', 'admin']
    },
    reset_password: {
        key: {
            type: String
        },
        expires_on: {
            type: Date
        }
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});
/*END: schema definition*/

/*START: methods*/
userSchema.methods.validatePassword = function (password)
{
    return bcrypt.compareSync(password, this.password);
};
/*END: methods*/

/*START:instantiate the model*/
var User = mongoose.model('user', userSchema);
/*END:instantiate the model*/

/*START: schema validations*/
userSchema.pre('save', function(next) {
    next();
});
/*END: schema validations*/

/*START: export schema*/
module.exports = User;
/*END: export schema*/