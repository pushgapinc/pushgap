/*START: module includes*/
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
/*END: module includes*/

/*START: schema definition*/
var segmentSchema = new Schema({
    workspace_id: {
        required: ['invalid workspace id.'],
        type: Schema.Types.ObjectId,
        ref: 'workspace'
    },
    application_id: {
        required: ['invalid application id.'],
        type: Schema.Types.ObjectId,
        ref: 'application'
    },
    name: {
        type: String,
        required: ['segment name is required.']
    },
    definition: {
        conditions: [{
            filter: {
                type: String
            },
            operator: {
                type: String
            },
            value: {
                type: String
            }
        }]
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});
/*END: schema definition*/

/*START:instantiate the model*/
var Segment = mongoose.model('segment', segmentSchema);
/*END:instantiate the model*/

/*START: export schema*/
module.exports = Segment;
/*END: export schema*/