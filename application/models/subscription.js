/*START: module includes*/
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
/*END: module includes*/

/*START: schema definition*/
var subscriptionSchema = new Schema({
    endpoint: {
        type: String,
        required: ['endpoint is required.']
    },
    subscription_id: {
        type: String,
        required: ['subscription_id is required.']
    },
    key: {
        type: String
    },
    auth: {
        type: String
    },
    browser: {
        type: String,
        required: ['browser is required.']
    },
    is_mobile: {
        type: Boolean,
        default: false
    },
    workspace_id: {
        required: ['invalid workspace id.'],
        type: Schema.Types.ObjectId,
        ref: 'workspace'
    },
    application_id: {
        required: ['invalid application id.'],
        type: Schema.Types.ObjectId,
        ref: 'application'
    },
    active: {
        type: Boolean,
        required: true,
        default: true
    },
    tags: {
        type: [String],
        validate: {
            validator: function(arr) {
                return arr.length <= 2;
            }
        }
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});
/*END: schema definition*/

/*START:instantiate the model*/
var Subscription = mongoose.model('subscription', subscriptionSchema);
/*END:instantiate the model*/

/*START: export schema*/
module.exports = Subscription;
/*END: export schema*/