/*START: module includes*/
var path = require('path');
var bcrypt = require('bcrypt-nodejs');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
/*END: module includes*/

/*START: schema definition*/
var inviteSchema = new Schema({
    email: {
        type: String,
        required: ['username is required.']
    },
    workspace_id: {
        type: Schema.Types.ObjectId,
        ref: 'workspace',
        required: ['invalid workspace id.']
    },
    invite_key: {
        type: String,
        required: ['invalid invite key.']
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});
/*END: schema definition*/

/*START:instantiate the model*/
var Invite = mongoose.model('invite', inviteSchema);
/*END:instantiate the model*/

/*START: export schema*/
module.exports = Invite;
/*END: export schema*/