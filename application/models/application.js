/*START: module includes*/
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
/*END: module includes*/

/*START: schema definition*/
var applicationSchema = new Schema({
    name: {
        type: String,
        required: ['application name is required.']
    },
    type: {
        type: String,
        required: ['application type is required.'],
        enum: ['webpush'],
        default: 'webpush'
    },
    domain: {
        type: String,
        unique: true,
        required: ['application domain is required.']
    },
    gcm_sender_id: {
        type: String,
        required: ['application gcm_sender_id is required.']
    },
    gcm_api_key: {
        type: String,
        required: ['application gcm_api_key is required.']
    },
    website: {
        protocol: {
            type: String,
            required: ['protocol is required.']
        },
        url: {
            type: String,
            required: ['domain is required.']
        }
    },
    workspace_id: {
        required: ['invalid workspace id.'],
        type: Schema.Types.ObjectId,
        ref: 'workspace'
    },
    active: {
        type: Boolean,
        required: true,
        default: true
    },
    logo: {
        type: String,
        required: ['application logo is required.']
    },
    welcome_notification: {
        enabled: {
            type: Boolean,
            default: false
        },
        title: {
            type: String,
            default: 'Welcome'
        },
        message: {
            type: String,
            default: 'Thanks for Subscribing to us.'
        },
        url: {
            type: String
        }
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});
/*END: schema definition*/

/*START:instantiate the model*/
var Application = mongoose.model('application', applicationSchema);
/*END:instantiate the model*/

/*START: schema validations*/
Application.schema.path('name').validate(function(name) {
    if (name.length < 3) {
        return false;
    }
    return true;
},'application name should be more than 3 characters.');

Application.schema.path('name').validate(function(name) {
    if (name.length > 15) {
        return false;
    }
    return true;
},'application name should be less than 15 characters.');
/*END: schema validations*/

/*START: export schema*/
module.exports = Application;
/*END: export schema*/