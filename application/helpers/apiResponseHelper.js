var apiResponseHelper = {};

/*START: setResponse*/
apiResponseHelper.setResponse = function (flag, message, payload, errorPayload)
{
	apiResponse = {};
	apiResponse.result = {};
	apiResponse.payload = {};

	apiResponse.result.success = flag;
	apiResponse.result.message = message;
	apiResponse.result.error = errorPayload;
	apiResponse.payload = payload;

	return apiResponse;
};
/*END: setResponse*/

/*START: sendResponse*/
apiResponseHelper.sendResponse = function(responseObject, status, flag, message, payload, errorPayload) {
	responseObject.status(status).send(apiResponseHelper.setResponse(flag, message, payload, errorPayload));
};
/*START: sendResponse*/

module.exports = apiResponseHelper;