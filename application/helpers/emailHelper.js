/*START: includes*/
var emailHelper = {};
var path = require('path');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var config = require(path.join(__dirname, '..', 'config', 'config'));
var winston = require(path.join(__dirname, '..', 'utils', 'winston'));
/*END: includes*/

/*START:smtp transporter*/
emailHelper.smtpTransporter = nodemailer.createTransport(smtpTransport({
    host: config.email.smtp.host,
    port: config.email.smtp.port,
    auth: {
        user: config.email.smtp.username,
        pass: config.email.smtp.password
    }
}));
/*END:smtp transporter*/

/*START: sendMail*/
/*emailPayload - {}
* .from:
* .to:
* .subject:
* .text:
* */
emailHelper.sendMail = function (emailPayload)
{
    if (!emailPayload) {
        return;
    }
    if (!emailPayload.from) {
        emailPayload.from = config.email.settings.defaultEmail;
    }
    if (!emailPayload.to) {
        return;
    }
    emailHelper.smtpTransporter.sendMail(emailPayload, function(error, response) {
        if (error) {
            winston.warn(error);
        } else {
            winston.info('Email sent');
        }
    });
};
/*END: setResponse*/


module.exports = emailHelper;