var helpers = angular.module('helpers', []);
helpers.factory('notifyService', [function() {
    var api = {};
    var config = {};
    config.delay = 3000;

    api.show = function(type, message, delay) {
        $.notify({
            message: message
        },{
            type: type,
            delay: delay || config.delay,
            placement: {
                from: "top",
                align: "center"
            },
            z_index: 9999
        });
    };

    return api;
}]);
helpers.factory('helperUtils', [function() {
    var api = {};

    api.containsSpecialChars = function(str) {

        if (!/[^a-zA-Z0-9]/.test(str)) {
            return false;
        }
        return true;
    };

    api.showConfirmationPopup = function (title,label,msg,type,callback)
    {
        var config = {};
        config.title = title;
        config.message = msg;
        config.buttons = {};

        if(type == 'delete')
        {
            config.buttons.danger = {
                label: label,
                className: "btn-warning",
                callback:callback
            };
        }

        if(type == 'generic')
        {
            config.buttons.generic = {
                label: label,
                className: "btn-info",
                callback:callback
            };
        }

        bootbox.dialog(config);
    };

    return api;
}]);
helpers.directive('formatDate', ['$filter', function ($filter)
{
    var directive = {};

    directive.link = function (scope, element, attrs)
    {
        scope.$watch(function() {
            return attrs.date;
        }, function() {
            $(element).html($filter('date')(attrs.date, 'dd/MMMM/yyyy'));
        });
    };

    return directive;
}]);