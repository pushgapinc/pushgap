var action = angular.module('action', ['ui.router', 'helpers']);

action.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', function ($stateProvider, $urlRouterProvider, $httpProvider) {

    $stateProvider.state('signup',
        {
            url: '/signup',
            templateUrl: tenant.server.host + '/resources/js/angular/modules/action/templates/signup.html',
            controller: 'signupController'
        })
        .state('emailconfirmation',
        {
            url: '/emailconfirmation/:key',
            templateUrl: tenant.server.host + '/resources/js/angular/modules/action/templates/emailconfirmation.html',
            controller: 'emailconfirmationController'
        });
    $urlRouterProvider.otherwise('/signup');
}]);

action.run(['$rootScope', '$state', function($rootScope, $state) {
    $rootScope.tenant = tenant;
    $rootScope.app = app;
}]);