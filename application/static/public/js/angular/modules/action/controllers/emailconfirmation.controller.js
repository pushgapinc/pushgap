action.controller('emailconfirmationController', ['$scope', 'notifyService', '$http', '$rootScope', '$timeout', '$stateParams', function($scope, notifyService, $http, $rootScope, $timeout, $stateParams) {
    $scope.registrationKey = $stateParams.key;
    $scope.activationSuccessful = false;
    $http(
        {
            method: 'PATCH',
            url: $rootScope.tenant.server.api + '/v1/workspaces/activate?key=' + $scope.registrationKey
        }).success(function (data, status, headers, config)
        {
            if (data.result.success) {
                $scope.activationSuccessful = true;
            }
            else {
                notifyService.show('danger', data.result.message);
            }
        }).error(function (data, status, headers, config)
        {
            notifyService.show('danger', data.result.message);
        });

}]);