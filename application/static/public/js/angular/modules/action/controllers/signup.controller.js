action.controller('signupController', ['$scope', 'notifyService', '$http', '$rootScope', '$timeout', 'helperUtils', function($scope, notifyService, $http, $rootScope, $timeout, helperUtils) {
    $scope.workspace = {};
    $scope.signUp = function() {
        if (!$scope.workspace.name) {
            notifyService.show('danger', 'invalid workspace name.');
            return;
        }
        if (!$scope.workspace.username) {
            notifyService.show('danger', 'invalid username.');
            return;
        }
        if (!$scope.workspace.password) {
            notifyService.show('danger', 'invalid password.');
            return;
        }
        if ($scope.workspace.password != $scope.workspace.confirmPassword) {
            notifyService.show('danger', 'password mismatch.');
            return;
        }
        if (helperUtils.containsSpecialChars($scope.workspace.name)) {
            notifyService.show('danger', 'special characters are not allowed in the workspace name.');
            return;
        }
        $http(
            {
                method: 'POST',
                data: $scope.workspace,
                url: $rootScope.tenant.server.api + '/v1/workspaces'
            }).success(function (data, status, headers, config)
            {
                if (data.result.success) {
                    var msg = 'Account created successfully.please check your mailbox.please wait while we redirect to your instance ......';
                    notifyService.show('info', msg, 10000);
                    $timeout(function() {
                        window.location = $scope.tenant.server.api.replace("api", $scope.workspace.name);
                    }, 3000);
                }
                else {
                    notifyService.show('danger', data.result.message);
                }
            }).error(function (data, status, headers, config)
            {
                if (data.result.error && data.result.error.code == 11000) {
                    notifyService.show('danger', 'workspace with the name "' + $scope.workspace.name + '" already' +
                        ' exists.');
                }
                else {
                    notifyService.show('danger', data.result.message);
                }
            });
    };
}]);