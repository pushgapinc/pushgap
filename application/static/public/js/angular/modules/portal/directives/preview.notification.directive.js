portal.directive('previewNotification', [function() {
    var directive = {};
    directive.scope = {
        title: '=',
        message: '=',
        logo: '=',
        protocol: '=',
        subdomain: '=',
        maindomain: '='
    };
    directive.restrict = 'E';
    directive.templateUrl = tenant.server.host + '/resources/js/angular/modules/portal/directives/templates/previewNotification.html';
    return directive;
}]);