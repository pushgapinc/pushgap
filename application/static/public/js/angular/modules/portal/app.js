var portal = angular.module('portal', ['ui.router', 'helpers']);

portal.factory('authTokenInterceptor', ['$injector','$rootScope', function($injector, $rootScope) {
    var api = {};
    api.request = function (config)
    {
        $rootScope.loading ++;
        var authToken = $injector.get('authService').getAuthToken();
        if (authToken) {
            config.headers['Authorization'] = authToken;
        }
        if ($rootScope.tenant && $rootScope.tenant.workspace && $rootScope.tenant.workspace.name) {
            config.headers['workspace'] = $rootScope.tenant.workspace.name;
        }
        return config;
    };
    api.requestError = function (config)
    {
        $rootScope.loading --;
        return config;
    };
    api.response = function(response) {
        $rootScope.loading --;
        return response;
    };
    api.responseError = function(response) {
        $rootScope.loading --;
        if (response.data.result && response.data.result.error && response.data.result.error.code === 'logout') {
            $injector.get('authService').logout();
            $injector.get('$state').go('login');
            response.data.result.message = 'session timed out.';
        }
        return response;
    };
    return api;
}]);

portal.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', function ($stateProvider, $urlRouterProvider, $httpProvider) {

    $httpProvider.interceptors.push('authTokenInterceptor');

    $stateProvider.state('login',
        {
            url: '/login',
            templateUrl: tenant.server.host + '/resources/js/angular/modules/portal/templates/login.html',
            controller: 'loginController'
        })
        .state('forgotpassword',
        {
            url: '/forgotpassword',
            templateUrl: tenant.server.host + '/resources/js/angular/modules/portal/templates/forgotpassword.html',
            controller: 'forgotpasswordController'
        })
        .state('resetpassword',
        {
            url: '/resetpassword',
            templateUrl: tenant.server.host + '/resources/js/angular/modules/portal/templates/resetpassword.html',
            controller: 'resetpasswordController'
        })
        .state('invite',
        {
            url: '/invite',
            templateUrl: tenant.server.host + '/resources/js/angular/modules/portal/templates/invite.html',
            controller: 'inviteController'
        })
        .state('console',
        {
            url: '/console',
            templateUrl: tenant.server.host + '/resources/js/angular/modules/portal/templates/console.html',
            controller: 'consoleController',
            requireAuthentication: true,
            redirectTo: 'console.dashboard',
            resolve: {
                user: function(userService) {
                    return userService.getUserDetails();
                }
            }
        })
        .state('console.dashboard',
        {
            url: '/dashboard',
            templateUrl: tenant.server.host + '/resources/js/angular/modules/portal/templates/console.dashboard.html',
            requireAuthentication: true,
            redirectTo: 'console.dashboard.applications'
        })
        .state('console.dashboard.profile',
        {
            url: '/profile',
            templateUrl: tenant.server.host + '/resources/js/angular/modules/portal/templates/console.dashboard.profile.html',
            requireAuthentication: true,
            controller: 'consoleDashboardProfileController'
        })
        .state('console.dashboard.invites',
        {
            url: '/invites',
            templateUrl: tenant.server.host + '/resources/js/angular/modules/portal/templates/console.dashboard.invites.html',
            requireAuthentication: true,
            controller: 'consoleDashboardInvitesController'
        })
        .state('console.dashboard.usermanagement',
        {
            url: '/usermanagement',
            templateUrl: tenant.server.host + '/resources/js/angular/modules/portal/templates/console.dashboard.usermanagement.html',
            requireAuthentication: true,
            controller: 'consoleDashboardUsermanagementController'
        })
        .state('console.dashboard.applications',
        {
            url: '/applications',
            template: '<ui-view></ui-view>',
            requireAuthentication: true,
            redirectTo: 'console.dashboard.applications.home'
        })
        .state('console.dashboard.applications.home',
        {
            url: '/',
            templateUrl: tenant.server.host + '/resources/js/angular/modules/portal/templates/console.dashboard.applications.home.html',
            requireAuthentication: true,
            controller: 'consoleDashboardApplicationsHomeController'
        })
        .state('console.dashboard.applications.app',
        {
            url: '/:appId',
            templateUrl: tenant.server.host + '/resources/js/angular/modules/portal/templates/console.dashboard.applications.app.html',
            requireAuthentication: true,
            controller: 'consoleDashboardApplicationsAppController',
            redirectTo: 'console.dashboard.applications.app.dashboard'
        })
        .state('console.dashboard.applications.app.dashboard',
        {
            url: '/dashboard',
            templateUrl: tenant.server.host + '/resources/js/angular/modules/portal/templates/console.dashboard.applications.app.dashboard.html',
            requireAuthentication: true,
            controller: 'consoleDashboardApplicationsAppDashboardController'
        })
        .state('console.dashboard.applications.app.pushnotification',
        {
            url: '/pushnotification',
            templateUrl: tenant.server.host + '/resources/js/angular/modules/portal/templates/console.dashboard.applications.app.pushnotification.html',
            requireAuthentication: true,
            controller: 'consoleDashboardApplicationsAppPushnotificationController'
        })
        .state('console.dashboard.applications.app.clientcode',
        {
            url: '/clientcode',
            templateUrl: tenant.server.host + '/resources/js/angular/modules/portal/templates/console.dashboard.applications.app.clientcode.html',
            requireAuthentication: true,
            controller: 'consoleDashboardApplicationsAppClientcodeController'
        })
        .state('console.dashboard.applications.app.settings',
        {
            url: '/settings',
            templateUrl: tenant.server.host + '/resources/js/angular/modules/portal/templates/console.dashboard.applications.app.settings.html',
            requireAuthentication: true,
            controller: 'consoleDashboardApplicationsAppSettingsController'
        })
        .state('console.dashboard.applications.app.notifications',
        {
            url: '/notifications',
            templateUrl: tenant.server.host + '/resources/js/angular/modules/portal/templates/console.dashboard.applications.app.notifications.html',
            requireAuthentication: true,
            controller: 'consoleDashboardApplicationsAppNotificationsController'
        })
        .state('console.dashboard.applications.app.segmentation',
        {
            url: '/segmentation',
            templateUrl: tenant.server.host + '/resources/js/angular/modules/portal/templates/console.dashboard.applications.app.segmentation.html',
            requireAuthentication: true,
            controller: 'consoleDashboardApplicationsAppSegmentationController'
        });
    $urlRouterProvider.otherwise('/login');
}]);

portal.run(['$rootScope', 'authService', '$state', function($rootScope, authService, $state) {
    $rootScope.tenant = tenant;
    $rootScope.app = app;
    $rootScope.loading = 0;
    $rootScope.tenant.server.sdk = $rootScope.tenant.server.api.replace('//api.', '//sdk.');

    authService.tryLoginFromSession();

    /*$stateChangeStart*/
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        if (toState.requireAuthentication) {
            if (!authService.isLogged()) {
                event.preventDefault();
                $state.go('login');
            }
        }
        if (toState.redirectTo) {
            event.preventDefault();
            $state.go(toState.redirectTo, toParams, {location: 'replace'})
        }
    });
    /*$stateChangeStart*/
}]);