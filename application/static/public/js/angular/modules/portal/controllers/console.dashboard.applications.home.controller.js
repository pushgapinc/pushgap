portal.controller('consoleDashboardApplicationsHomeController', ['$scope', 'authService', '$rootScope', 'notifyService', '$state', 'applicationsService','$timeout', 'helperUtils', function($scope, authService, $rootScope, notifyService, $state, applicationsService, $timeout, helperUtils) {

    $scope.init = function() {
        applicationsService.getApplications($rootScope.tenant.workspace.id).then(function(applications) {
            $scope.applications = angular.copy(applications);
        }, function(data) {
            notifyService.show('danger', data.result.message);
        });
    };
    $scope.init();

    $scope.openCreateAppModal = function() {
        $scope.newApp = {};
        $scope.newApp.website = {};
        $scope.newApp.welcome_notification = {};
        $scope.newApp.welcome_notification.enabled = true;
        $scope.newApp.website.protocol= 'http';
        $('.newApp').modal('show');
    };

    $scope.createApplication = function() {
        if (!$scope.newApp.name) {
            notifyService.show('danger', 'invalid application name.');
            return;
        }
        if (!$scope.newApp.website.protocol) {
            notifyService.show('danger', 'invalid website protocol.');
            return;
        }
        if (!$scope.newApp.website.url) {
            notifyService.show('danger', 'invalid website url.');
            return;
        }
        if (!$scope.newApp.logo) {
            notifyService.show('danger', 'invalid logo.');
            return;
        }
        if (!$scope.newApp.domain) {
            notifyService.show('danger', 'invalid domain name.');
            return;
        }
        if (!$scope.newApp.gcm_sender_id) {
            notifyService.show('danger', 'invalid gcm_sender_id.');
            return;
        }
        if (!$scope.newApp.gcm_api_key) {
            notifyService.show('danger', 'invalid gcm_api_key.');
            return;
        }
        if ($scope.newApp.welcome_notification.enabled) {
            if (!$scope.newApp.welcome_notification.title) {
                notifyService.show('danger', 'invalid welcome notification title.');
                return;
            }
            if (!$scope.newApp.welcome_notification.message) {
                notifyService.show('danger', 'invalid welcome notification message.');
                return;
            }
            if (!$scope.newApp.welcome_notification.url) {
                notifyService.show('danger', 'invalid welcome notification target url.');
                return;
            }
        }
        applicationsService.createApplication($rootScope.tenant.workspace.id, $scope.newApp).then(function(data) {
            $scope.applications.push(data.payload);
            $timeout(function() {
                $('.newApp').modal('hide');
            });
        }, function(data) {
            if (data.result.error && data.result.error.code == 11000) {
                notifyService.show('danger', $scope.newApp.domain + ' - domain is not available.');
            }
            else {
                notifyService.show('danger', data.result.message);
            }
        });
    };

    $scope.deleteApplication = function(application) {
        helperUtils.showConfirmationPopup('Delete Application ?', 'Delete', 'Are you sure you want to delete this Application - ' + application.name+ ' ? Note that all the existing subscriptions and notifications will be deleted permanently and cannot be recovered. Do you want to continue ?', 'delete', function() {
            applicationsService.deleteApplication($rootScope.tenant.workspace.id, application._id).then(function(data) {
                $scope.applications = _.reject($scope.applications, function(app){ return app._id == application._id; });
                notifyService.show('info', "Application deleted successfully.");
            }, function(data) {
                notifyService.show('danger', data.result.message);
            });
        });
    };
}]);