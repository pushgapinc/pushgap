portal.controller('consoleDashboardApplicationsAppNotificationsController', ['$scope', 'authService', '$rootScope', 'notifyService', '$state', 'applicationsService','$http', function($scope, authService, $rootScope, notifyService, $state, applicationsService, $http) {

    $scope.notifications = [];
    $scope.page = 1;
    $scope.loadMore = true;
    $scope.getNotifications = function() {
        $http(
            {
                method: 'GET',
                url: $rootScope.tenant.server.api + '/v1/workspaces/' + $rootScope.tenant.workspace.id + '/applications/' + $scope.currentAppId + '/notifications?page=' + $scope.page
            }).success(function (data, status, headers, config)
            {
                if (data.result.success) {
                    angular.forEach(data.payload, function(notification) {
                        if (notification.click_count == 0 || notification.receive_count == 0) {
                            notification.click_rate = 0;
                        }
                        else {
                            notification.click_rate = (notification.click_count/notification.receive_count) * 100;
                        }
                        $scope.notifications.push(notification);
                    });
                    if (data.payload && data.payload.length == 0) {
                        $scope.loadMore = false;
                    }
                    $scope.page += 1;
                }
                else {
                    notifyService.show('danger', data.result.message);
                }
            }).error(function (data, status, headers, config)
            {
                notifyService.show('danger', data.result.message);
            });
    };
    $scope.getNotifications();
}]);