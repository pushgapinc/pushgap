portal.controller('consoleDashboardApplicationsAppClientcodeController', ['$scope', 'authService', '$rootScope', 'notifyService', '$state', 'applicationsService','$http', function($scope, authService, $rootScope, notifyService, $state, applicationsService, $http) {

    $scope.copyScript = function() {
        var clientCode = angular.element('#clientCode');

        var textArea = document.createElement("textarea");
        textArea.style.position = 'fixed';
        textArea.style.top = 0;
        textArea.style.left = 0;
        textArea.style.width = '2em';
        textArea.style.height = '2em';
        textArea.style.padding = 0;
        textArea.style.border = 'none';
        textArea.style.outline = 'none';
        textArea.style.boxShadow = 'none';
        textArea.style.background = 'transparent';
        textArea.value = clientCode.text();
        document.body.appendChild(textArea);
        textArea.select();

        try {
            document.execCommand('copy');
            notifyService.show('info', 'copied to clipboard.');
        } catch (err) {
            notifyService.show('info', 'press ctrl+c to copy.');
        }
    };
}]);