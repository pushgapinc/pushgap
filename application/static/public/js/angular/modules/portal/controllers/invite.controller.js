portal.controller('inviteController', ['$scope', 'authService', '$rootScope', 'notifyService', '$http','$location', '$state', 'helperUtils', function($scope, authService, $rootScope, notifyService, $http, $location, $state, helperUtils) {
    $scope.invite = {};
    $scope.invite.email = $location.search()['email'];

    $scope.acceptInvite = function() {
        if (!$scope.invite.email) {
            notifyService.show('danger', 'invalid email.');
            return;
        }
        if (!$scope.invite.password) {
            notifyService.show('danger', 'invalid password.');
            return;
        }
        if ($scope.invite.password != $scope.invite.confirmPassword) {
            notifyService.show('danger', 'password mismatch.');
            return;
        }
        $http(
            {
                method: 'POST',
                data: $scope.invite,
                url: $rootScope.tenant.server.api + '/v1/workspaces/' + $rootScope.tenant.workspace.id + '/acceptinvite?key=' + $location.search()['key']
            }).success(function (data, status, headers, config)
            {
                if (data.result.success) {
                    notifyService.show('info', data.result.message);
                    $state.go('login');
                }
                else {
                    notifyService.show('danger', data.result.message);
                }
            }).error(function (data, status, headers, config)
            {
                notifyService.show('danger', data.result.message);
            });
    };
}]);