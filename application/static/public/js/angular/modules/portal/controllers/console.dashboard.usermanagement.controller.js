portal.controller('consoleDashboardUsermanagementController', ['$scope', 'authService', '$rootScope', 'notifyService', '$state', 'applicationsService','$http', 'helperUtils', '$timeout', function($scope, authService, $rootScope, notifyService, $state, applicationsService, $http, helperUtils, $timeout) {

    $scope.users = [];

    $http(
        {
            method: 'GET',
            url: $rootScope.tenant.server.api + '/v1/workspaces/' + $rootScope.tenant.workspace.id + '/users'
        }).success(function (data, status, headers, config)
        {
            if (data.result.success) {
                $scope.users = data.payload;
            }
            else {
                notifyService.show('danger', data.result.message);
            }
        }).error(function (data, status, headers, config)
        {
            notifyService.show('danger', data.result.message);
        });

    $scope.deleteUser = function(user) {
        helperUtils.showConfirmationPopup('Delete User ?', 'Delete', 'Are you sure you want to delete this user ' + user.username+ ' ?', 'delete', function() {
            $http(
                {
                    method: 'DELETE',
                    url: $rootScope.tenant.server.api + '/v1/workspaces/' + $rootScope.tenant.workspace.id + '/users/' + user._id
                }).success(function (data, status, headers, config)
                {
                    if (data.result.success) {
                        notifyService.show('info', data.result.message);
                        $timeout(function() {
                            $scope.users = _.reject($scope.users, function(eachUser){ return eachUser._id == user._id; });
                        });
                    }
                    else {
                        notifyService.show('danger', data.result.message);
                    }
                }).error(function (data, status, headers, config)
                {
                    notifyService.show('danger', data.result.message);
                });
        });
    };

}]);