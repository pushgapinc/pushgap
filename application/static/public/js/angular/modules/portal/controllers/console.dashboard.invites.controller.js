portal.controller('consoleDashboardInvitesController', ['$scope', 'authService', '$rootScope', 'notifyService', '$state', 'applicationsService','$http', 'helperUtils', '$timeout', function($scope, authService, $rootScope, notifyService, $state, applicationsService, $http, helperUtils, $timeout) {

    $scope.invites = [];

    $http(
        {
            method: 'GET',
            url: $rootScope.tenant.server.api + '/v1/workspaces/' + $rootScope.tenant.workspace.id + '/invites'
        }).success(function (data, status, headers, config)
        {
            if (data.result.success) {
                $scope.invites = data.payload;
            }
            else {
                notifyService.show('danger', data.result.message);
            }
        }).error(function (data, status, headers, config)
        {
            notifyService.show('danger', data.result.message);
        });

    $scope.deleteInvite = function(invite) {
        helperUtils.showConfirmationPopup('Delete Invite ?', 'Delete', 'Are you sure you want to delete this invite to ' + invite.email+ ' ?', 'delete', function() {
            $http(
                {
                    method: 'DELETE',
                    url: $rootScope.tenant.server.api + '/v1/workspaces/' + $rootScope.tenant.workspace.id + '/invites/' + invite._id
                }).success(function (data, status, headers, config)
                {
                    if (data.result.success) {
                        notifyService.show('info', data.result.message);
                        $timeout(function() {
                            $scope.invites = _.reject($scope.invites, function(invitation){ return invitation._id == invite._id; });
                        });
                    }
                    else {
                        notifyService.show('danger', data.result.message);
                    }
                }).error(function (data, status, headers, config)
                {
                    notifyService.show('danger', data.result.message);
                });
        });
    };

    $scope.resendInvite = function(invite) {
        helperUtils.showConfirmationPopup('Resend Invite ?', 'Yes', 'Are you sure you want to resend invite to ' + invite.email+ ' ?', 'generic', function() {
            $http(
                {
                    method: 'PUT',
                    url: $rootScope.tenant.server.api + '/v1/workspaces/' + $rootScope.tenant.workspace.id + '/invites/' + invite._id
                }).success(function (data, status, headers, config)
                {
                    if (data.result.success) {
                        notifyService.show('info', data.result.message);
                    }
                    else {
                        notifyService.show('danger', data.result.message);
                    }
                }).error(function (data, status, headers, config)
                {
                    notifyService.show('danger', data.result.message);
                });
        });
    };

}]);