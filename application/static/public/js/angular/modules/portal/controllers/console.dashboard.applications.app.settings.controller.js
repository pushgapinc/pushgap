portal.controller('consoleDashboardApplicationsAppSettingsController', ['$scope', 'authService', '$rootScope', 'notifyService', '$state', 'applicationsService','$http', function($scope, authService, $rootScope, notifyService, $state, applicationsService, $http) {

    $scope.application = angular.copy($scope.currentApplication);
    $scope.$watch('currentApplication', function(newVal, oldVal) {
        $scope.application = angular.copy($scope.currentApplication);
    });
    $scope.updateApplication = function() {
        if (!$scope.application.name) {
            notifyService.show('danger', 'invalid application name.');
            return;
        }
        if (!$scope.application.website.protocol) {
            notifyService.show('danger', 'invalid website protocol.');
            return;
        }
        if (!$scope.application.website.url) {
            notifyService.show('danger', 'invalid website url.');
            return;
        }
        if (!$scope.application.logo) {
            notifyService.show('danger', 'invalid logo.');
            return;
        }
        if (!$scope.application.domain) {
            notifyService.show('danger', 'invalid domain name.');
            return;
        }
        if (!$scope.application.gcm_sender_id) {
            notifyService.show('danger', 'invalid gcm_sender_id.');
            return;
        }
        if (!$scope.application.gcm_api_key) {
            notifyService.show('danger', 'invalid gcm_api_key.');
            return;
        }
        applicationsService.updateApplication($rootScope.tenant.workspace.id, $scope.application).then(function(data) {
            notifyService.show('info', 'settings saved successfully.');
            $state.reload();
        }, function(data) {
            notifyService.show('danger', data.result.message);
        });
    };
    $scope.resetSettings = function() {
        $scope.application = angular.copy($scope.currentApplication);
    };
}]);