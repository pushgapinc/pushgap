portal.controller('forgotpasswordController', ['$scope', 'authService', '$rootScope', 'notifyService', '$state','$http', function($scope, authService, $rootScope, notifyService, $state, $http) {
    $scope.user = {};
    $scope.user.username = "";
    $scope.requestResetPassword = function() {
        if (!$scope.user.username) {
            notifyService.show('danger', 'invalid username.');
            return;
        }
        $http(
            {
                method: 'POST',
                data: app,
                url: $rootScope.tenant.server.api + '/v1/workspaces/' + $rootScope.tenant.workspace.id + '/users/password/reset?username=' + $scope.user.username
            }).success(function (data, status, headers, config)
            {
                if (data.result.success) {
                    notifyService.show('info', data.result.message);
                }
                else {
                    notifyService.show('danger', data.result.message);
                }
            }).error(function (data, status, headers, config)
            {
                notifyService.show('danger', data.result.message);
            });
    };
}]);