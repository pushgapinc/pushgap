portal.controller('consoleDashboardProfileController', ['$scope', 'authService', '$rootScope', 'notifyService', '$state', 'applicationsService','$http', function($scope, authService, $rootScope, notifyService, $state, applicationsService, $http) {

    $scope.getApiSecret = function() {
        $http(
            {
                method: 'GET',
                url: $rootScope.tenant.server.api + '/v1/workspaces/' + $rootScope.tenant.workspace.id + '/apisecret/'
            }).success(function (data, status, headers, config)
            {
                if (data.result.success) {
                    $scope.apiSecret = data.payload;
                }
                else {
                    notifyService.show('danger', data.result.message);
                }
            }).error(function (data, status, headers, config)
            {
                notifyService.show('danger', data.result.message);
            });
    };
    $scope.getApiSecret();
}]);