portal.controller('consoleController', ['$scope', 'authService', '$rootScope', 'notifyService', '$state', 'user', '$http','userService', function($scope, authService, $rootScope, notifyService, $state, user, $http, userService) {
    $scope.logout = function() {
        authService.logout();
        userService.clearUser();
        $state.go('login');
    };
    $scope.user = angular.copy(user);
    $scope.showInviteUserPopup = function() {
        $scope.invite = {};
        $('.inviteUser').modal('show');
    };
    $scope.inviteUser = function() {
        if (!$scope.invite.email) {
            notifyService.show('danger', 'invalid email id.');
            return;
        }
        $http(
            {
                method: 'POST',
                data: $scope.invite,
                url: $rootScope.tenant.server.api + '/v1/workspaces/' + $rootScope.tenant.workspace.id + '/invites'
            }).success(function (data, status, headers, config)
            {
                if (data.result.success) {
                    $('.inviteUser').modal('hide');
                    notifyService.show('info', 'invite sent successfully.');
                }
                else {
                    notifyService.show('danger', data.result.message);
                }
            }).error(function (data, status, headers, config)
            {
                notifyService.show('danger', data.result.message);
            });
    };
}]);