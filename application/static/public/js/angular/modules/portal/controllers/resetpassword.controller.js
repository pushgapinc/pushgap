portal.controller('resetpasswordController', ['$scope', '$location', '$rootScope', 'notifyService', '$state','$http', function($scope, $location, $rootScope, notifyService, $state, $http) {
    $scope.user = {};
    $scope.resetPassword = function() {
        if (!$scope.user.newPassword) {
            notifyService.show('danger', 'invalid password.');
            return;
        }
        if ($scope.user.newPassword != $scope.user.confirmPassword) {
            notifyService.show('danger', 'password\'s mismatch.');
            return;
        }
        $http(
            {
                method: 'PUT',
                data: $scope.user,
                url: $rootScope.tenant.server.api + '/v1/users/password?key=' + $location.search().key
            }).success(function (data, status, headers, config)
            {
                if (data.result.success) {
                    notifyService.show('info', data.result.message);
                    $state.go('login');
                }
                else {
                    notifyService.show('danger', data.result.message);
                }
            }).error(function (data, status, headers, config)
            {
                notifyService.show('danger', data.result.message);
            });
    };
}]);