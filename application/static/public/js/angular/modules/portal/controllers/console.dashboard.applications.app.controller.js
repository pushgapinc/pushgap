portal.controller('consoleDashboardApplicationsAppController', ['$scope', 'authService', '$rootScope', 'notifyService', '$state', 'applicationsService','$stateParams', function($scope, authService, $rootScope, notifyService, $state, applicationsService, $stateParams) {

    $scope.currentAppId = $stateParams.appId;
    $scope.currentApplication = {};
    $scope.getApplication = function(appId) {
        applicationsService.getApplication($rootScope.tenant.workspace.id, appId).then(function(application) {
            $scope.currentApplication = angular.copy(application);
        }, function(data) {
            notifyService.show('danger', data.result.message);
        });
    };

    $scope.getApplication($scope.currentAppId);
}]);