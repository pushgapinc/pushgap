portal.controller('consoleDashboardApplicationsAppPushnotificationController', ['$scope', 'authService', '$rootScope', 'notifyService', '$state', 'applicationsService','$http','segmentationService', function($scope, authService, $rootScope, notifyService, $state, applicationsService, $http, segmentationService) {

    $scope.notification = {};
    $scope.notification.icon = $scope.currentApplication.logo;
    $scope.notification.segmentation = {};
    $scope.notification.segmentation.type = 'all';
    $scope.queryParams = [];
    $scope.segments = [];

    $scope.filters = segmentationService.getFilters();

    $scope.addCondition = function(segment) {
        var condition = {};
        condition.filter = 'browser';
        $scope.setPossibleOperator(condition);
        if (!segment.definition) {
            segment.definition = {};
        }
        if (!segment.definition.conditions) {
            segment.definition.conditions = [];
        }
        segment.definition.conditions.push(condition);
    };

    $scope.getSegments = function() {
        segmentationService.getApplicationSegments($rootScope.tenant.workspace.id, $scope.currentAppId).then(function(segments) {
            $scope.segments = angular.copy(segments);
        }, function(data) {
            notifyService.show('danger', data.result.message);
        });
    };
    $scope.getSegments();

    $scope.$watch('currentApplication.logo', function() {
        $scope.notification.icon = $scope.currentApplication.logo;
    });

    $scope.$watch('notification.useQueryParams', function() {
        if (!$scope.notification.useQueryParams) {
            $scope.queryParams = [];
        }
    });

    $scope.$watch('notification.segmentation.type', function(oldVal, newVal) {
        $scope.notification.segmentation.name = $scope.notification.segmentation.type;
    });

    $scope.setPossibleOperator = function(condition) {
        delete condition.operator;
        delete condition.value;
        for (var index = 0; index< $scope.filters.length; index++) {
            var filter = $scope.filters[index];
            if (filter.id == condition.filter) {
                condition.possibleOperators = angular.copy(filter.operators);
                condition.possibleValues = angular.copy(filter.values);
                break;
            }
        }
    };

    $scope.pushNotificationConfirmation = function() {
        if (!$scope.notification.title) {
            notifyService.show('danger', 'invalid notification title.');
            return;
        }
        if (!$scope.notification.message) {
            notifyService.show('danger', 'invalid notification message.');
            return;
        }
        if (!$scope.notification.target_location) {
            notifyService.show('danger', 'invalid target location.');
            return;
        }

        if ($scope.notification.segmentation.type == 'existing_segment') {
            if (!$scope.notification.segmentation.segment_id) {
                notifyService.show('danger', 'please select an existing segment.');
                return;
            }
            for (var index = 0; index < $scope.segments.length; index++) {
                var segment = $scope.segments[index];
                if (segment._id == $scope.notification.segmentation.segment_id) {
                    $scope.notification.segmentation.definition = segment.definition;
                }
            }
        }

        if ($scope.notification.segmentation.type == 'custom_segment') {
            if (!segmentationService.validateSegmentConditions($scope.notification.segmentation)) {
                return;
            }
        }

        $('.pushNotification').modal('show');
    };

    $scope.getSegmentSubscriptionCount = function(segment) {
        if (!segmentationService.validateSegmentConditions(segment)) {
            return;
        }

        segmentationService.getSubscriptionCount($rootScope.tenant.workspace.id, $scope.currentAppId, segment).then(function(count) {
            notifyService.show('info', count);
        }, function(data) {
            notifyService.show('danger', data.result.message);
        });
    };

    $scope.pushNotification = function() {
        var notificationPayload = angular.copy($scope.notification);
        notificationPayload.target_location = $scope.getTargetLocation();
        $http(
            {
                method: 'POST',
                data: notificationPayload,
                url: $rootScope.tenant.server.api + '/v1/workspaces/' + $rootScope.tenant.workspace.id + '/applications/' + $scope.currentAppId + '/notifications'
            }).success(function (data, status, headers, config)
            {
                if (data.result.success) {
                    notifyService.show('info', 'notification sent successfully.');
                    $('.pushNotification').modal('hide');
                }
                else {
                    notifyService.show('danger', data.result.message);
                }
            }).error(function (data, status, headers, config)
            {
                notifyService.show('danger', data.result.message);
            });
    };

    $scope.getTargetLocation = function() {
        if (!$scope.notification.target_location) {
            return "";
        }
        var target_location = $scope.notification.target_location;
        if ($scope.getQueryParamsAsUrl()) {
            target_location = $scope.notification.target_location + '?' + $scope.getQueryParamsAsUrl();
        }
        return target_location;
    };
    $scope.getQueryParamsAsUrl = function() {
        var paramsString = '';
        angular.forEach($scope.queryParams, function(param) {
            if (param.key) {
                paramsString = paramsString + param.key + '=' + param.value + '&';
            }
        });
        return paramsString.slice(0, -1);
    };

    $scope.previewNotification = function() {
        if (Notification.permission !== "granted")
            Notification.requestPermission().then(function(permission) {
                var notification = new Notification($scope.notification.title, {
                    body: $scope.notification.message,
                    icon: $scope.currentApplication.logo
                });

                notification.onclick = function () {

                    var target_location = $scope.getTargetLocation();
                    window.open(target_location);
                    notification.close();
                };
            });
        else {
            if (!$scope.notification.title) {
                notifyService.show('danger', 'invalid notification title.');
                return;
            }
            if (!$scope.notification.message) {
                notifyService.show('danger', 'invalid notification message.');
                return;
            }
            if (!$scope.notification.target_location) {
                notifyService.show('danger', 'invalid target location.');
                return;
            }
            var notification = new Notification($scope.notification.title, {
                body: $scope.notification.message,
                icon: $scope.currentApplication.logo
            });

            notification.onclick = function () {

                var target_location = $scope.getTargetLocation();
                window.open(target_location);
                notification.close();
            };

        }
    };

    $scope.addQueryParam = function() {
        var param = {};
        param.key = '';
        param.value = '';
        $scope.queryParams.push(param);
    };

    $scope.setNotificationIcon = function() {
        $scope.notification.icon = $scope.currentApplication.logo;
    };
}]);