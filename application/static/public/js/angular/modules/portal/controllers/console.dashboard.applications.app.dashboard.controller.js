portal.controller('consoleDashboardApplicationsAppDashboardController', ['$scope', 'authService', '$rootScope', 'notifyService', '$state', 'applicationsService','subscriptionsService','$http', function($scope, authService, $rootScope, notifyService, $state, applicationsService, subscriptionsService, $http) {

    $scope.subscriptionsCount = {};
    subscriptionsService.getSubscriptionsCount($rootScope.tenant.workspace.id, $scope.currentAppId).then(function(subscriptionsCount) {
        $scope.subscriptionsCount = angular.copy(subscriptionsCount);
    }, function(data) {
        notifyService.show('danger', data.result.message);
    });

    $scope.getLastNotification = function() {
        $http(
            {
                method: 'GET',
                url: $rootScope.tenant.server.api + '/v1/workspaces/' + $rootScope.tenant.workspace.id + '/applications/' + $scope.currentAppId + '/notifications/latest'
            }).success(function (data, status, headers, config)
            {
                if (data.result.success) {
                    $scope.lastNotification = data.payload;
                    if ($scope.lastNotification) {
                        if ($scope.lastNotification.click_count == 0 || $scope.lastNotification.receive_count == 0) {
                            $scope.lastNotification.click_rate = 0;
                        }
                        else {
                            $scope.lastNotification.click_rate = ($scope.lastNotification.click_count/$scope.lastNotification.receive_count) * 100;
                        }
                    }
                }
                else {
                    notifyService.show('danger', data.result.message);
                }
            }).error(function (data, status, headers, config)
            {
                notifyService.show('danger', data.result.message);
            });
    };
    $scope.getLastNotification();
}]);