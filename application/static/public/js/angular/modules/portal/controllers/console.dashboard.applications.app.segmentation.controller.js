portal.controller('consoleDashboardApplicationsAppSegmentationController', ['$scope', '$timeout', '$rootScope', 'notifyService', '$state', 'applicationsService','$http','helperUtils', 'segmentationService', function($scope, $timeout, $rootScope, notifyService, $state, applicationsService, $http, helperUtils, segmentationService) {
    $scope.segments = [];
    $scope.filters = segmentationService.getFilters();

    $scope.setPossibleOperator = function(condition) {
        delete condition.operator;
        delete condition.value;
        for (var index = 0; index< $scope.filters.length; index++) {
            var filter = $scope.filters[index];
            if (filter.id == condition.filter) {
                condition.possibleOperators = angular.copy(filter.operators);
                condition.possibleValues = angular.copy(filter.values);
                break;
            }
        }
    };

    $scope.addSegment = function(segment) {
        if (!segmentationService.validateSegmentConditions(segment)) {
            return;
        }
        segmentationService.addSegment($rootScope.tenant.workspace.id, $scope.currentAppId, segment).then(function(newSegment){
            $scope.segments.push(newSegment);
            notifyService.show('info', 'segment created successfully.');
            $timeout(function() {
                $('.newSegment').modal('hide');
            });
        }, function(data) {
            notifyService.show('danger', data.result.message);
        });
    };

    $scope.updateSegment = function(segment) {

        if (!segmentationService.validateSegmentConditions(segment)) {
            return;
        }
        segmentationService.updateSegment($rootScope.tenant.workspace.id, $scope.currentAppId, segment).then(function(segments){
            notifyService.show('info', 'segment updated successfully.');
            $scope.segments = angular.copy(segments);
            $timeout(function() {
                $('.updateSegment').modal('hide');
            });
        }, function(data) {
            notifyService.show('danger', data.result.message);
        });

    };

    $scope.getSegmentSubscriptionCount = function(segment) {
        if (!segmentationService.validateSegmentConditions(segment)) {
            return;
        }

        segmentationService.getSubscriptionCount($rootScope.tenant.workspace.id, $scope.currentAppId, segment).then(function(count) {
            notifyService.show('info', count);
        }, function(data) {
            notifyService.show('danger', data.result.message);
        });

    };

    $scope.deleteSegment = function(segment) {

        helperUtils.showConfirmationPopup('Delete Segment ?', 'Delete', 'Are you sure you want to delete this segment ' + segment.name + ' ?', 'delete', function () {
            segmentationService.deleteSegment($rootScope.tenant.workspace.id, $scope.currentAppId, segment).then(function(segments){
                $scope.segments = angular.copy(segments);
                notifyService.show('info', 'segment deleted successfully.');
            }, function(data) {
                notifyService.show('danger', data.result.message);
            });

        });
    };



    $scope.getSegments = function() {
        segmentationService.getApplicationSegments($rootScope.tenant.workspace.id, $scope.currentAppId).then(function(segments) {
            $scope.segments = angular.copy(segments);
        }, function(data) {
            notifyService.show('danger', data.result.message);
        });
    };
    $scope.getSegments();

    $scope.openCreateSegmentModal = function() {
        $scope.newSegment = {};
        $scope.newSegment.definition = {};
        $scope.newSegment.name = "";
        $scope.newSegment.definition.conditions = [];
        $timeout(function() {
            $('.newSegment').modal('show');
        });
    };
    $scope.viewSegment = function(segment) {
        $scope.currentSegment = angular.copy(segment);
        for (var index = 0; index < $scope.currentSegment.definition.conditions.length; index++) {
            var condition = $scope.currentSegment.definition.conditions[index];
            for (var index1 = 0; index1< $scope.filters.length; index1++) {
                var filter = $scope.filters[index1];
                if (filter.id == condition.filter) {
                    condition.possibleOperators = angular.copy(filter.operators);
                    condition.possibleValues = angular.copy(filter.values);
                    break;
                }
            }
        }
        $timeout(function() {
            $('.updateSegment').modal('show');
        });
    };

    $scope.addCondition = function(segment) {
        var condition = {};
        condition.filter = 'browser';
        $scope.setPossibleOperator(condition);
        if (!segment.definition.conditions) {
            segment.definition.conditions = [];
        }
        segment.definition.conditions.push(condition);
    };
}]);