portal.controller('loginController', ['$scope', 'authService', '$rootScope', 'notifyService', '$state','userService', function($scope, authService, $rootScope, notifyService, $state, userService) {
    $scope.init = function() {
        $scope.user = {};
    };
    $scope.login = function(username, password) {
        if (!username) {
            notifyService.show('danger', 'invalid username.');
            return;
        }
        if (!password) {
            notifyService.show('danger', 'invalid password.');
            return;
        }

        authService.login($rootScope.tenant.workspace.id, username, password).then(function() {
            $state.go('console');
        }, function(data) {
            notifyService.show('danger', data.result.message);
        });
    };
    $scope.init();
}]);