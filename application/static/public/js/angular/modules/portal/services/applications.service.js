portal.factory('applicationsService', ['$http', '$rootScope', '$q', function($http, $rootScope, $q) {
    var api = {};
    var applications = [];
    var applicationsFetched = false;

    api.getApplications = function(workspaceId) {

        var deferred = $q.defer();

        if (applicationsFetched) {
            deferred.resolve(applications);
            return deferred.promise;
        }

        $http(
            {
                method: 'GET',
                url: $rootScope.tenant.server.api + '/v1/workspaces/' + workspaceId + '/applications'
            }).success(function (data, status, headers, config)
            {
                if (data.result.success) {
                    applicationsFetched = true;
                    applications = data.payload;
                    deferred.resolve(applications);
                }
                else {
                    deferred.reject(data);
                }
            }).error(function (data, status, headers, config)
            {
                deferred.reject(data);
            });
        return deferred.promise;
    };

    api.getApplication = function(workspaceId, appId) {

        var deferred = $q.defer();

        for (var index = 0; index < applications.length; index++) {
            if (applications[index]._id == appId) {
                deferred.resolve(applications[index]);
                return deferred.promise;
            }
        }

        $http(
            {
                method: 'GET',
                url: $rootScope.tenant.server.api + '/v1/workspaces/' + workspaceId + '/applications/' + appId
            }).success(function (data, status, headers, config)
            {
                if (data.result.success) {
                    deferred.resolve(data.payload);
                }
                else {
                    deferred.reject(data);
                }
            }).error(function (data, status, headers, config)
            {
                deferred.reject(data);
            });
        return deferred.promise;
    };

    api.deleteApplication = function(workspaceId, appId) {

        var deferred = $q.defer();
        $http(
            {
                method: 'DELETE',
                url: $rootScope.tenant.server.api + '/v1/workspaces/' + workspaceId + '/applications/' + appId
            }).success(function (data, status, headers, config)
            {
                if (data.result.success) {
                    applications = _.reject(applications, function(application){ return application._id == appId; });
                    deferred.resolve();
                }
                else {
                    deferred.reject(data);
                }
            }).error(function (data, status, headers, config)
            {
                deferred.reject(data);
            });
        return deferred.promise;
    };

    api.createApplication = function(workspaceId, app) {
        var deferred = $q.defer();

        $http(
            {
                method: 'POST',
                data: app,
                url: $rootScope.tenant.server.api + '/v1/workspaces/' + workspaceId + '/applications'
            }).success(function (data, status, headers, config)
            {
                if (data.result.success) {
                    applications.push(data.payload);
                    deferred.resolve(data);
                }
                else {
                    deferred.reject(data);
                }
            }).error(function (data, status, headers, config)
            {
                deferred.reject(data);
            });

        return deferred.promise;
    };

    api.updateApplication = function(workspaceId, app) {
        var deferred = $q.defer();

        $http(
            {
                method: 'PUT',
                data: app,
                url: $rootScope.tenant.server.api + '/v1/workspaces/' + workspaceId + '/applications/' + app._id
            }).success(function (data, status, headers, config)
            {
                if (data.result.success) {
                    for (var index = 0; index < applications.length; index++) {
                        if (applications[index]._id == app._id) {
                            applications[index] = data.payload;
                            deferred.resolve(applications[index]);
                            return;
                        }
                    }
                    deferred.resolve(data.payload);
                }
                else {
                    deferred.reject(data);
                }
            }).error(function (data, status, headers, config)
            {
                deferred.reject(data);
            });

        return deferred.promise;
    };

    return api;
}]);