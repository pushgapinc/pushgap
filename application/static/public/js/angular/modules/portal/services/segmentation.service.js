portal.factory('segmentationService', ['$http', '$rootScope', '$q','notifyService', function($http, $rootScope, $q, notifyService) {
    var api = {};
    var filters = [];
    var segments = {};
    /*START: filters*/
    var browserFilter = {
        id: 'browser',
        name: 'browser',
        operators: [{
            id: 'is',
            name: 'is'
        }],
        values: [{
            id: 'chrome',
            name: 'chrome'
        }, {
            id: 'firefox',
            name: 'firefox'
        }]
    };
    var deviceFilter = {
        id: 'is_mobile',
        name: 'device',
        operators: [{
            id: 'is',
            name: 'is'
        }],
        values: [{
            id: "false",
            name: 'desktop'
        }, {
            id: "true",
            name: 'mobile'
        }]
    };
    var tagsFilter = {
        id: 'tags',
        name: 'tags',
        operators: [{
            id: 'containing_all',
            name: 'containing all'
        },{
            id: 'containing_one_of',
            name: 'containing one of'
        }]
    };
    filters.push(browserFilter);
    filters.push(deviceFilter);
    filters.push(tagsFilter);
    /*END: filters*/

    api.getFilters = function() {
        return filters;
    };

    api.getApplicationSegments = function(workspaceId, appId) {

        var deferred = $q.defer();

        if (segments[appId]) {
            deferred.resolve(segments[appId]);
            return deferred.promise;
        }

        $http(
            {
                method: 'GET',
                url: $rootScope.tenant.server.api + '/v1/workspaces/' + workspaceId + '/applications/' + appId + '/segments'
            }).success(function (data, status, headers, config)
            {
                if (data.result.success) {
                    segments[appId] = data.payload;
                    deferred.resolve(segments[appId]);
                }
                else {
                    deferred.reject(data);
                }
            }).error(function (data, status, headers, config)
            {
                deferred.reject(data);
            });
        return deferred.promise;
    };

    api.addSegment = function(workspaceId, appId, segment) {

        var deferred = $q.defer();
        $http(
            {
                method: 'POST',
                data: segment,
                url: $rootScope.tenant.server.api + '/v1/workspaces/' + workspaceId + '/applications/' + appId + '/segments'
            }).success(function (data, status, headers, config)
            {
                if (data.result.success) {
                    segments[appId].push(data.payload);
                    deferred.resolve(data.payload);
                }
                else {
                    deferred.reject(data);
                }
            }).error(function (data, status, headers, config)
            {
                deferred.reject(data);
            });
        return deferred.promise;
    };

    api.updateSegment = function(workspaceId, appId, segment) {

        var deferred = $q.defer();
        $http(
            {
                method: 'PUT',
                data: segment,
                url: $rootScope.tenant.server.api + '/v1/workspaces/' + workspaceId + '/applications/' + appId + '/segments/' + segment._id
            }).success(function (data, status, headers, config)
            {
                if (data.result.success) {
                    for (var index = 0; index< segments[appId].length; index++) {
                        if (segment._id == segments[appId][index]._id) {
                            segments[appId][index] = data.payload;
                            break;
                        }
                    }
                    deferred.resolve(segments[appId]);
                }
                else {
                    deferred.reject(data);
                }
            }).error(function (data, status, headers, config)
            {
                deferred.reject(data);
            });
        return deferred.promise;
    };

    api.deleteSegment = function(workspaceId, appId, segment) {

        var deferred = $q.defer();

        $http(
            {
                method: 'DELETE',
                url: $rootScope.tenant.server.api + '/v1/workspaces/' + workspaceId + '/applications/' + appId + '/segments/' + segment._id
            }).success(function (data, status, headers, config) {
                if (data.result.success) {
                    for (var index = 0; index < segments[appId].length; index++) {
                        if (segment._id == segments[appId][index]._id) {
                            segments[appId].splice(index, 1);
                            break;
                        }
                    }
                    deferred.resolve(segments[appId]);
                }
                else {
                    deferred.reject(data);
                }
            }).error(function (data, status, headers, config) {
                deferred.reject(data);
            });
        return deferred.promise;
    };

    api.validateSegmentConditions = function(segment) {
        if (!segment.name) {
            notifyService.show('danger', 'invalid segment name.');
            return 0;
        }
        if (!segment.definition) {
            notifyService.show('danger', 'invalid segment definition.');
            return 0;
        }
        if (!segment.definition.conditions) {
            notifyService.show('danger', 'segment should have at least one condition.');
            return 0;
        }
        if (segment.definition.conditions.length == 0) {
            notifyService.show('danger', 'segment should have at least one condition.');
            return 0;
        }
        for (var index = 0; index< segment.definition.conditions.length; index++) {
            var condition = segment.definition.conditions[index];
            if (!condition.filter) {
                notifyService.show('danger', 'condition number - ' + (index+1)  + ' have invalid filter.');
                return 0;
            }
            if (!condition.operator) {
                notifyService.show('danger', 'condition number - ' + (index+1)  + ' have invalid operator.');
                return 0;
            }
            if (condition.value == undefined || condition.value == null || condition.value === "") {
                notifyService.show('danger', 'condition number - ' + (index+1)  + ' have invalid value.');
                return 0;
            }
        }
        return 1;
    };

    api.getSubscriptionCount = function(workspaceId, appId, segment) {

        var deferred = $q.defer();
        $http(
            {
                method: 'POST',
                data: segment,
                url: $rootScope.tenant.server.api + '/v1/workspaces/' + workspaceId + '/applications/' + appId + '/segments/count'
            }).success(function (data, status, headers, config)
            {
                if (data.result.success) {
                    deferred.resolve(data.payload);
                }
                else {
                    deferred.reject(data);
                }
            }).error(function (data, status, headers, config)
            {
                deferred.reject(data);
            });
        return deferred.promise;
    };

    return api;
}]);