portal.factory('userService', ['$http', '$rootScope', '$q', function($http, $rootScope, $q) {
    var api = {};
    var user = {};
    var userDetailsFetched = false;

    api.getUserDetails = function(workspaceId) {

        var deferred = $q.defer();

        if (userDetailsFetched) {
            deferred.resolve(user);
            return deferred.promise;
        }

        $http(
            {
                method: 'GET',
                url: $rootScope.tenant.server.api + '/v1/workspaces/' + $rootScope.tenant.workspace.id + '/session/user'
            }).success(function (data, status, headers, config)
            {
                if (data.result.success) {
                    userDetailsFetched = true;
                    user = data.payload;
                    deferred.resolve(user);
                }
                else {
                    deferred.reject(data);
                }
            }).error(function (data, status, headers, config)
            {
                deferred.reject(data);
            });
        return deferred.promise;
    };

    api.clearUser = function() {
        userDetailsFetched = false;
    };

    return api;
}]);