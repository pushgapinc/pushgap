portal.factory('subscriptionsService', ['$http', '$rootScope', '$q', function($http, $rootScope, $q) {
    var api = {};
    var subscriptionsCount = {};

    api.getSubscriptionsCount = function(workspaceId, appId) {

        var deferred = $q.defer();

        if (subscriptionsCount[appId]) {
            deferred.resolve(subscriptionsCount[appId]);
            return deferred.promise;
        }

        $http(
            {
                method: 'GET',
                url: $rootScope.tenant.server.api + '/v1/workspaces/' + workspaceId + '/applications/' + appId + '/subscriptions/count'
            }).success(function (data, status, headers, config)
            {
                if (data.result.success) {
                    subscriptionsCount[appId] = data.payload;
                    deferred.resolve(subscriptionsCount[appId]);
                }
                else {
                    deferred.reject(data);
                }
            }).error(function (data, status, headers, config)
            {
                deferred.reject(data);
            });
        return deferred.promise;
    };

    return api;
}]);