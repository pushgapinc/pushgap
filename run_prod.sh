#!/bin/bash
export NODE_ENV=production
export NODE_PORT=8888
pm2 list
pm2 start app.js --name pushgap -e /root/logs/pushgap/error.log -o /root/logs/pushgap/output.log --log-date-format 'DD-MM HH:mm:ss.SSS'